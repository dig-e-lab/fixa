import os
import cv2
import imagehash
import ntpath

from datetime import datetime
from skimage.measure import compare_ssim
from PIL import Image

from Modules.drawing import save_as
from Modules.heatmap import get_heatmap_overlay
from Modules.file_utils import get_files_with_extension
from Modules.utils import generate_combinations

from common import checkDir

def get_gray_img(path):
    static_img = cv2.imread(path, cv2.IMREAD_COLOR)
    return cv2.cvtColor(static_img, cv2.COLOR_BGR2GRAY)


if __name__ == '__main__':
    ''' Use this script to know the similarity level between stimuli/pictures'''
    
    pictures_folder = '' # stimuli

    checkDir(pictures_folder)

    img_paths = get_files_with_extension(pictures_folder, extensions=['.png', ])

    combinations = generate_combinations(img_paths)

    for path1, path2 in combinations:
        img1 = cv2.imread(path1, cv2.IMREAD_COLOR)  # RGB to BGR
        img2 = cv2.imread(path2, cv2.IMREAD_COLOR)

        gray_a = cv2.cvtColor(img1, cv2.COLOR_RGB2GRAY)
        gray_b = cv2.cvtColor(img2, cv2.COLOR_RGB2GRAY)

        name_1, name_2 = ntpath.basename(path1), ntpath.basename(path2)
        print('{0} >< {1}'.format(name_1, name_2))

        """
        Image detection inside an image 
        False positive detection
        A possible way to handle it is to use all the detection methods and then try finding a match between every 
        algorithms. But ...
        """
        # from matplotlib import pyplot as plt
        # import numpy as np
        # import imutils
        #
        # method = cv2.TM_CCOEFF
        #
        # for scale in np.linspace(0.2, 1.0, 20)[::-1]:
        #     # resize the image according to the scale, and keep track
        #     # of the ratio of the resizing
        #     resized = imutils.resize(gray_b, width=int(gray_b.shape[1] * scale))
        #     r = gray_b.shape[1] / float(resized.shape[1])
        #
        #     w, h = resized.shape[::-1]
        #
        #     result = cv2.matchTemplate(gray_a, resized, method)
        #     loc = np.where(result >= 0.8)
        #
        #     min_val, max_val, min_loc, max_loc = cv2.minMaxLoc(result)
        #
        #     if method in [cv2.TM_SQDIFF, cv2.TM_SQDIFF_NORMED]:
        #         top_left = min_loc
        #     else:
        #         top_left = max_loc
        #     bottom_right = (top_left[0] + w, top_left[1] + h)
        #
        #     cv2.rectangle(img1, top_left, bottom_right, 255, 2)
        #
        #     plt.subplot(121), plt.imshow(result, cmap='gray')
        #     plt.title('Matching Result'), plt.xticks([]), plt.yticks([])
        #     plt.subplot(122), plt.imshow(img1, cmap='gray')
        #     plt.title('Detected Point'), plt.xticks([]), plt.yticks([])
        #
        #     plt.show()

        """
        Use hash methods
        """
        image_1, image_2 = Image.open(path1), Image.open(path2)

        _start = datetime.now()
        hash = imagehash.average_hash(image_1)
        score1 = hash - imagehash.average_hash(image_2)

        print('average_hash: {0} - {1}'.format(score1, datetime.now() - _start))

        _start = datetime.now()
        hash = imagehash.phash(image_1)
        score2 = hash - imagehash.phash(image_2)

        print('      phash : {0} - {1}'.format(score2, datetime.now() - _start))

        _start = datetime.now()
        hash = imagehash.dhash(image_1)
        score3 = hash - imagehash.dhash(image_2)

        print('      dhash : {0} - {1} => {2}'.format(score3, datetime.now() - _start, score1 + score2 + score3))

        _start = datetime.now()
        hash = imagehash.whash(image_1)
        score4 = hash - imagehash.whash(image_2)

        print('      whash : {0} - {1} => {2}'.format(score4, datetime.now() - _start, score2 + score3 + score4))

        _start = datetime.now()
        score, diff = compare_ssim(gray_a, gray_b, multichannel=False, gaussian_weights=True, full=True)

        print('       ssim : {0} - {1}'.format(score, datetime.now() - _start))
        print(' ssim(diff) : {0} - {1}'.format((1-score)*100, datetime.now() - _start))

        # diff => tab of similarity [0.0 .. 1.0]
        '''Tried to show the differences in the 2 images trough heatmap'''

        for i in range(len(diff)):
            for j in range(len(diff[i])):
                diff[i][j] = 255 - (255 * diff[i][j])

        diff[diff < 0] = 0
        diff = diff.astype('uint8')

        heat = get_heatmap_overlay(img1, diff, alpha=0.3, beta=0.7)
        # cv2.imshow('image', heat)
        # cv2.waitKey()

        # save_as(cv2.cvtColor(heat, cv2.COLOR_BGR2RGB), os.path.join(folder, 'SIM_{0}.png'.format(score)))
