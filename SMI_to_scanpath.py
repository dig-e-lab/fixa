from pathlib import Path

from Modules.file_utils import write_content_in_file, get_all_files, read_file_content

from Models.fixation import merge_both_eyes, FixationGenerator

from common import *

EXTENSION = '_scanpath.txt'


def _read_smi_data(file_path, header=False):

    _content = read_file_content(file_path, True, '\t')
    if header:
        content.remove(content[0])

    _data = [FixationGenerator.from_smi_log(float(d[9]), float(d[10]), float(d[6]), str(d[16]), float(d[8]))
             for d in _content]

    return _data


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Demonstration of landmarks localization.')
    parser.add_argument('-v', type=str, help='logging level', default='info',
                        choices=['debug', 'info', 'warning', 'error', 'critical'])
    parser.add_argument('--dir', type=str, help='Path of the directory that contains SMI files.')
    args = parser.parse_args()

    _dir = str(args.dir) # subjects fixations location
    
    checkDir(_dir)

    files = get_all_files(_dir)

    txt_files = [f for f in files if f.endswith('.txt') and not f.endswith(EXTENSION)]

    scanpath_dir = os.path.join(_dir, 'scanpath')
    os.makedirs(scanpath_dir, exist_ok=True)

    for source in txt_files:

        file_name = Path(source).stem

        data = _read_smi_data(source)
        # don't need FixationGenerator class
        blend = merge_both_eyes(data)

        dest = os.path.join(scanpath_dir, file_name + EXTENSION)
        content = ['{0} {1} {2} {3}'.format(fix.x, fix.y, round(fix.start/1000, 4), round(fix.end/1000, 4))
                   for fix in blend]

        write_content_in_file(dest, content)
