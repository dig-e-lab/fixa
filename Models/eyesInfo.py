from Modules.utils import timestamp_to_ticks


def replace_none_by_zero(value):
    if value is None:
        return 0
    else:
        return value


# def value_is_invalid(val):
#     return val is None or val < 0


class Eye:
    def __init__(self):
        self.x = None
        self.y = None
        # self.validity = -1
        self.diameter = None  # ?

    def is_valid(self):
        return self.x is not None and 0 <= self.x <= 1 and \
               self.y is not None and 0 <= self.y <= 1


class EyesInfo:
    def __init__(self, id_frame, timestamp):
        self.id = id_frame
        self.left = Eye()
        self.right = Eye()
        self._timestamp = timestamp
        self._convertedTime = 0
        self._localTime = 0
        self._ticks = int(timestamp_to_ticks(timestamp / 1000))

    def is_valid(self):
        return self.left.is_valid() and self.right.is_valid()

    def check_cross_gaze(self):
        if self.left.is_valid() and self.right.is_valid():
            if self.left.x > self.right.x:  # reverse eyes ?
                return True

        return False

    def __str__(self):
        return '{0} {1} {2} {3} {4}' \
            .format(replace_none_by_zero(self.left.x), replace_none_by_zero(self.left.y),
                    replace_none_by_zero(self.right.x), replace_none_by_zero(self.right.y),
                    self._ticks)
