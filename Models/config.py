import os


class Config:
    def __init__(self, json_data):
        self._config = json_data['config']

        self.acquisition_logs_dir = self._config['acquisition_logs_dir']
        self.video_gaze = self._config['video_gaze']
        self.output_analysis_dir = self._config['output_analysis_dir']

        self.steps = ConfigSteps(self._config)

        self.maps = ConfigMaps(self._config)

        self.analysis = ConfigAnalysis(self._config)

        assert os.path.isdir(self.acquisition_logs_dir)
        assert os.path.isdir(self.output_analysis_dir)


class ConfigSteps:
    def __init__(self, json_data):
        self._config = json_data['steps']

        self.stimuli_dir = self._config['stimuli_dir']
        self.start_stop_dir = self._config['start_stop_dir']

        self.threshold_by_picture = self._config['threshold_by_picture']
        self.mode = self._config['mode']

        _tmp = self._config['threshold']
        self.threshold = int(_tmp) if _tmp else None


class ConfigMaps:
    def __init__(self, json_data):
        self._config = json_data['maps']

        self.fixations_dir = self._config['fixations_dir']
        self.output_fm_dir = self._config['output_fm_dir']
        self.output_sm_dir = self._config['output_sm_dir']


class ConfigAnalysis:
    def __init__(self, json_data):
        self._config = json_data['analysis']

        self.opacity_map_needed = self._config['opacity_map_needed']
        self.fixation_metrics_needed = self._config['fixation_metrics_needed']
        self.interval_needed = self._config['interval_needed']
        self.scanpath_video_needed = self._config['scanpath_video_needed']
        self.spatial_metrics_needed = self._config['spatial_metrics_needed']
        self.ref_group = self._config['ref_group']
