import numpy as np
import pickle
import cv2

import Modules.drawing as draw

from copy import copy


def get_areas_occupation(areas, _str):
    _dict = {}

    for z in areas:
        fixation_count = len(_str)

        occupation = _str.count(z.name)/fixation_count
        _dict[z.name] = occupation

    return _dict


def generate_grid_format_aoi(n, width, height):
    areas = []
    cw = width / n
    ch = height / n
    count = 0
    for i in range(n):

        for j in range(n):
            z = AOI(chr(97 + count), (cw*j, ch*i), (cw*(j+1), ch*(i+1)))
            areas.append(z)
            count += 1

    return areas


def string_edit(fixations, areas):
    _string = ''
    for fix in fixations:
        fixation = fix.astype(np.int32)
        for aoi in areas:
            if aoi.is_point_inside((fixation[0], fixation[1])):
                _string += aoi.name
                break

    return _string


def get_areas_text_default_values(areas):
    d = {}
    for z in areas:
        d[z.name] = z.name

    return d


def show_areas_on_image(img, areas, areas_text, path):  # need BGR image
    color = (0, 0, 255)
    tmp = copy(img)

    for z in areas:
        x1, y1 = z.top_left
        x2, y2 = z.bottom_right
        text = str(areas_text[z.name])

        padding_x = (40 * (len(text)/2))  # try to place text at the center as much as possible

        center_x = int(x1 + (x2 - x1)//2 - padding_x)
        center_y = y1 + (y2 - y1)//2 + 40

        cv2.line(tmp, (x1, y1), (x2, y1), color, thickness=6, lineType=8, shift=0)
        cv2.line(tmp, (x2, y2), (x2, y2), color, thickness=6, lineType=8, shift=0)
        cv2.line(tmp, (x1, y1), (x1, y2), color, thickness=6, lineType=8, shift=0)
        cv2.line(tmp, (x2, y1), (x2, y2), color, thickness=6, lineType=8, shift=0)
        cv2.putText(tmp, text, (center_x, center_y), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0),
                    thickness=8, lineType=cv2.LINE_AA)

    draw.save_as(tmp, path)


class AOI:
    def __init__(self, name, top_left, bottom_right):
        self.name = name
        self.top_left = top_left
        self.bottom_right = bottom_right

    def is_point_inside(self, point):
        result = False
        px, py = point

        tlx, tly = self.top_left
        br_x, br_y = self.bottom_right

        if tlx <= px < br_x and tly <= py < br_y:
            result = True

        return result

    def __str__(self):
        return '{0} -> {1}{2}'.format(self.name, self.top_left, self.bottom_right)


def dump_areas(file, obj):
    with open(file, mode='wb') as f:
        pickle.dump(obj, f)


def from_file(file):  # DO NOT FORGET to import the class in the calling script
    with open(file, mode='rb') as f:
        from_file = pickle.load(f)

    return from_file



