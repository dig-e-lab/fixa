class Step:
    def __init__(self, name):
        self.name = name
        self.start = 0.0
        self.end = 0.0
        self.frame = None
        self.logs = []

    def __str__(self):
        return '{0} {1} {2}'.format(self.name, self.start, self.end)

    def started(self):
        return self.start > 0.0

    def is_valid(self):
        return self.start < self.end and self.end > 0.0

    @classmethod
    def from_file(cls, line):
        _args = line.strip().split(' ')
        _stp = cls(_args[0])
        _stp.start = float(_args[1])
        _stp.end = float(_args[2])

        if not _stp.is_valid():
            raise IOError('The step end before it start.({0})'.format(_stp.name))

        return _stp


class StepV2:
    def __init__(self, name):
        self.name = name
        self._start = 0.0
        self.started = False
        self.frame = None
        self.logs = []
        self.apparition_intervals = []

    def __str__(self):
        _str = '{0}'.format(self.name)

        for _t in self.apparition_intervals:
            _str += ' {0}'.format(str(_t))

        return _str

    def set_start(self, val):
        self._start = val
        self.started = True

    def set_end_step(self, val):
        # in order to handle discontinuity of the step apparitions (pop-up, rollback)
        self.apparition_intervals.append((self._start, val))
        self._start = 0.0
        self.started = False

    @classmethod
    def from_file(cls, line):
        _args = line.strip().split(' ')
        _stp = cls(_args[0])
        _stp.start = float(_args[1])
        _stp.end = float(_args[2])  # NEED TO UPDATE THIS

        return _stp


class StepHash(Step):
    def __init__(self, name):
        super().__init__(name)

        self.average_hash = None
        self.phash = None
        self.dhash = None
        self.whash = None
        self.threshold = None


class StepInfo(Step):
    def __init__(self, name, frame, count):
        super().__init__(name)
        self.frame = frame
        self.subject_count = count
        self.fixations_by_user = {}

    def is_valid(self):
        return self.subject_count == len(self.fixations_by_user.keys())


def dump_steps(file_path, steps):

    if len(steps) == 0:
        return  # no steps to be saved

    with open(file_path, mode='w') as file:
        for s in steps:
            if s.is_valid():
                file.write(str(s) + '\n')


def load_steps_info_file(file_path):
    stps = []
    with open(file_path, mode='r') as file:
        for l in file:
            stps.append(Step.from_file(l))

    return stps
