from Modules.utils import ticks_to_timestamp
from Modules.file_utils import read_file_content


def read_raw_data_file(path, to_be_merged=True):
    # read file with x, y data format

    def _process_line(args):
        _tmp = int(args[-1]) if len(args[-1]) == 18 else int(args[-1])*10
        _time = ticks_to_timestamp(_tmp)

        return [args[0], args[1], args[2], args[3], _time]

    content = read_file_content(path, True)

    if to_be_merged:
        logs = [LogMerged(_process_line(data)) for data in content]
    else:
        logs = [Log(_process_line(data)) for data in content]

    return logs


def dummy_log(time):
    return LogMerged([-1, -1, -1, -1, time])


class GazePoint:
    # Used as container for raw data
    # Default: x and y should have a value in the range [0-1]
    def __init__(self, x, y, time, max_width=1, max_height=1):
        self.x = float(x)*max_width
        self.y = float(y)*max_height
        self.time = float(time)  # timestamp

        self.is_valid = 0 < self.x < max_width and 0 < self.y < max_height

    @classmethod
    def from_gp(cls, gp, max_width, max_height):
        return cls(gp.x, gp.y, gp.time, max_width, max_height)


class Log:
    # First step to classify raw data from files
    # args : xL yL xR yR time(timestamp)
    def __init__(self, args_list, w=1920, h=1080):
        self.gp_left = GazePoint(args_list[0], args_list[1], args_list[4], max_width=w, max_height=h)
        self.gp_right = GazePoint(args_list[2], args_list[3], args_list[4], max_width=w, max_height=h)
        self.time = args_list[4]

    def is_valid(self):
        return self.gp_left.is_valid and self.gp_right.is_valid


class LogMerged:
    # First step to classify raw data from files
    # args : xL yL xR yR time(timestamp)
    def __init__(self, args):
        x = (float(args[0]) + float(args[2])) / 2
        y = (float(args[1]) + float(args[3])) / 2
        self.gp = self._special_process(GazePoint(x, y, args[4]))

        self.time = args[4]

    def is_valid(self):
        return self.gp.is_valid

    def _special_process(self, _gp):
        if not _gp.is_valid:
            _gp.x = -1.0
            _gp.y = -1.0
        return _gp

    def get_gp_pixel(self, w=1920, h=1080):
        return self._special_process(GazePoint.from_gp(self.gp, max_width=w, max_height=h))
