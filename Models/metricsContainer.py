from numpy import mean
from scipy import stats


def _exclude_value(items):
    return [item for item in items if item is not None]


class MetricsContainer:
    def __init__(self):
        """ Edit Distance """
        self.ED = 0
        """ String Edit Distance"""
        self.SED = 0
        """ Time Delay Embedding Distance (distance mode: mean) """
        self.TDED_3_mean = 0
        self.TDED_5_mean = 0
        """ Time Delay Embedding Distance (distance mode: Hausdorff) """
        self.TDED_3_Hausdorff = 0
        self.TDED_5_Hausdorff = 0

        self.occupation_by_zone = {}

    def init_occupation(self, d1, d2):

        #  d1 and d2 must have same keys
        for k in d1:
            self.occupation_by_zone[k] = [d1[k], d2[k]]

    def get_metrics_val(self):
        return self.ED, self.SED, self.TDED_3_mean, self.TDED_5_mean, self.TDED_3_Hausdorff, \
               self.TDED_5_Hausdorff

    def __str__(self):
        return 'ED={0} SED={1} TDED_3_mean={2} TDED_5_mean={3}, TDED_3_Hausdorff={4}, TDED_5_Hausdorff={5}'.format(
            self.ED, self.SED, self.TDED_3_mean, self.TDED_5_mean, self.TDED_3_Hausdorff,
            self.TDED_5_Hausdorff)


class MeanContainer(MetricsContainer):

    def __init__(self, metrics_data):
        """
            Fuse a list of MetricsContainer in one instance of MetricsContainer
            :param metrics_data: list of MetricsContainer
            """
        super().__init__()

        if len(metrics_data) == 0:
            raise Exception('No metrics was passed to be computed.')
        '''
        keeping this method for the ED because using based_fixation_mean() we have to cut part of one of the two arrays
        unless they are the same length
        '''
        bunch = [(mean(md.ED), md.SED, md.TDED_3_mean, md.TDED_5_mean, md.TDED_3_Hausdorff, md.TDED_5_Hausdorff)
                 for md in metrics_data]
        _ed, _sed, _tded, _tded_5, _tded_h, _tded_5_h = zip(*bunch)

        _tded, _tded_5, _tded_h, _tded_5_h = _exclude_value(_tded), _exclude_value(_tded_5), _exclude_value(_tded_h), \
                                             _exclude_value(_tded_5_h)

        for md in metrics_data:
            for k in md.occupation_by_zone:
                if k in self.occupation_by_zone:
                    self.occupation_by_zone[k].extend(md.occupation_by_zone[k])
                else:
                    self.occupation_by_zone[k] = md.occupation_by_zone[k]

        self.ED = mean(_ed)
        self.SED = mean(_sed)

        self.TDED_3_mean = mean(_tded)
        self.TDED_5_mean = mean(_tded_5)
        self.TDED_3_Hausdorff = mean(_tded_h)
        self.TDED_5_Hausdorff = mean(_tded_5_h)

        for k in self.occupation_by_zone:
            _m = mean(self.occupation_by_zone[k])
            self.occupation_by_zone[k] = round(_m, 3)


class StandardErrorContainer(MetricsContainer):

    def __init__(self, metrics_data):
        super().__init__()
        """
            The standard error of a statistic is the standard deviation of its sampling distribution
            or an estimate of that standard deviation.
        """

        bunch = [(mean(md.ED), md.SED, md.TDED_3_mean, md.TDED_5_mean, md.TDED_3_Hausdorff, md.TDED_5_Hausdorff)
                 for md in metrics_data]
        _ed, _sed, _tded, _tded_5, _tded_h, _tded_5_h = zip(*bunch)

        self.ED = stats.sem(_ed)
        self.SED = stats.sem(_sed)
        self.TDED_3_mean = stats.sem(_exclude_value(_tded))
        self.TDED_5_mean = stats.sem(_exclude_value(_tded_5))
        self.TDED_3_Hausdorff = stats.sem(_exclude_value(_tded_h))
        self.TDED_5_Hausdorff = stats.sem(_exclude_value(_tded_5_h))
