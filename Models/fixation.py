from Models.gazepoint import GazePoint
from Modules.file_utils import read_file_content

MIN_FIXATION_TIME = 0.100  # seconds


def read_fixation_file(source):
    content = read_file_content(source, True)
    from_file = [FixationGenerator.from_log(float(data[0]), float(data[1]), float(data[2]), float(data[3]))
                 for data in content]

    return from_file


def merge_both_eyes(data):
    processing, fused_data = None, []

    copy_data = [d for d in data]

    for i in range(len(copy_data)):
        actual_fix = copy_data[i]

        if processing is None:  # first iteration
            processing = FixationBlend(actual_fix)
            continue

        if processing.data.eye_side != actual_fix.eye_side \
                and actual_fix.start < processing.data.end:

            processing.add_data(actual_fix)

        else:
            processing.validate()  # apply mean for x and y
            fused_data.append(processing.data)
            processing = FixationBlend(actual_fix)

        if i + 1 == len(copy_data):  # last iteration
            processing.validate()  # apply mean for x and y
            fused_data.append(processing.data)

    return fused_data


def process_logs_into_fixations(logs, radius):
    _ref_time = logs[0].time
    left_fixations = ScanpathGenerator(radius, 'left')
    right_fixations = ScanpathGenerator(radius, 'right')

    for log in logs:
        # Calibrate time (0.0 -> end)
        log.gp_left.time = log.gp_left.time - _ref_time
        log.gp_right.time = log.gp_right.time - _ref_time

        left_fixations.process_gaze_point(log.gp_left)
        right_fixations.process_gaze_point(log.gp_right)

    data = []
    index_l, index_r = 0, 0
    remain_left, remain_right = True, True

    while remain_left or remain_right:
        left, right = None, None
        if remain_left:
            remain_left, left = left_fixations.try_get_next_fixation(index_l)

        if remain_right:
            remain_right, right = right_fixations.try_get_next_fixation(index_r)

        if remain_left and remain_right:
            if left.start < right.start:
                data.append(left)
                index_l += 1
            else:
                data.append(right)
                index_r += 1
        else:
            if remain_left:
                data.append(left)
                index_l += 1
            elif remain_right:
                data.append(right)
                index_r += 1

    return merge_both_eyes(data)


class FixationGenerator:
    def __init__(self, first_gaze_point, side):
        self.x = first_gaze_point.x
        self.y = first_gaze_point.y
        self.start = first_gaze_point.time
        self.end = first_gaze_point.time
        self.eye_side = side
        self.duration = None

    def add_gaze_point(self, gp):
        self.x = (self.x + gp.x) / 2
        self.y = (self.y + gp.y) / 2
        self.end = gp.time

    def get_fixation_duration(self):  # in milliseconds
        return self.end - self.start

    @classmethod
    def from_log(cls, x, y, start, end):
        fix = cls(GazePoint(x, y, start), 'both')
        fix.end = end

        return fix

    @classmethod
    def from_smi_log(cls, x, y, time, side, end):
        """ Special case : SMI provides fixation from the get-go"""
        fix = cls(GazePoint(x, y, time), side)
        fix.end = end

        return fix


class ScanpathGenerator:
    def __init__(self, radius, side):
        self.radius = radius
        self.fixations = []
        self.actual_fixation = None
        self.eye = side

    def process_gaze_point(self, gp):
        """ first attempt
        Problem : using centroid of a group of points is wrong because the common zone will just change position,
        not size
        BUT in fact, the more point there is, the smallest the common zone will be.
        """

        if not gp.is_valid:  # ignore detection error
            return

        if self.actual_fixation is None:
            self.actual_fixation = FixationGenerator(gp, self.eye)
            return

        from_same_fixation = self._point_in_circle((gp.x, gp.y))

        if from_same_fixation:
            self.actual_fixation.add_gaze_point(gp)
        else:
            fix_duration = self.actual_fixation.get_fixation_duration()

            if fix_duration >= MIN_FIXATION_TIME:
                self.fixations.append(self.actual_fixation)

            self.actual_fixation = FixationGenerator(gp, self.eye)

    def try_get_next_fixation(self, index):
        _success = False
        _fix = None

        if 0 <= index < len(self.fixations):
            _fix = self.fixations[index]  # try a queue !!!!!!!!!!!!!!!!!
            _success = True

        return _success, _fix

    def _point_in_circle(self, point):
        """ Detect if a given gaze point can be assimilated to the actual fixation"""
        x, y = point

        dx = abs(x - self.actual_fixation.x)
        dy = abs(y - self.actual_fixation.y)

        return dx ** 2 + dy ** 2 <= self.radius ** 2


class FixationBlend:
    """Fuses fixations from both eyes into one
    """

    def __init__(self, data):

        self.data = data
        self.x_list = [self.data.x, ]
        self.y_list = [self.data.y, ]

        self.dx = {data.eye_side: [self.data.x, ], }
        self.dy = {data.eye_side: [self.data.y, ], }

    def add_data(self, data):
        _side = data.eye_side

        if self.data.end < data.end:
            self.data.end = data.end

        self.data.duration = round(self.data.end - self.data.start, 1)
        self.data.eye_side = 'Both'

        self.x_list.append(data.x)
        self.y_list.append(data.y)

        # HANDLE new key
        if data.eye_side in self.dx:
            self.dx[_side].append(data.x)
        else:
            self.dx[_side] = [data.x, ]

        if _side in self.dy:
            self.dy[_side].append(data.y)
        else:
            self.dy[_side] = [data.y, ]

    def validate(self):
        self.data.x = self._mean(self.x_list)
        self.data.y = self._mean(self.y_list)

    def _mean(self, values, round_precision=1):
        tmp = sum([val for val in values])
        return round(tmp / len(values), round_precision)
