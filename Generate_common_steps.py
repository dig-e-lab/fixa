import ntpath
import cv2

from skimage.measure import compare_ssim
from PIL import Image

from Models.step import load_steps_info_file, dump_steps, StepInfo

from Modules.raw_data_manipulation import retrieve_fixations_by_steps
from Modules.file_utils import get_acquisition_logs_from_group_dir, read_config
from Modules.maps_utils import generate_saliency_map, generate_fixations_map
from Modules.steps_from_video import auto_slice_into_steps

from common import *


def get_log_info(log_dir):
    steps = load_steps_info_file(join(log_dir, STEPS_FILE_NAME))
    step_paths = [join(log_dir, st.name + '.png') for st in steps]
    fix_files = _get_fixation_files(log_dir)

    return log_dir, step_paths, fix_files


def _get_fixation_files(dir_path):
    _tmp = []
    for r, d, f in os.walk(dir_path):
        for _f in f:
            if FIXATIONS_FILE_EXTENSION in _f and PREFIX_STEP in _f:
                _tmp.append(_f)
    return _tmp


def retrieve_common_step(log_directories):
    """
    Compare each steps between acquisition logs to identify where is the fewer steps
    These steps are more likely the step that will be found in all other acquisition
    """

    common_steps, tmp = [], []
    min_count = 1000
    subject_count = len(log_directories)

    for log_dir in log_directories:
        # steps = load_steps_info_file(join(log_dir, STEPS_FILE_NAME))
        # step_paths = [join(log_dir, st.name + '.png') for st in steps]

        steps, step_paths, _ = get_log_info(log_dir)

        if min_count > len(step_paths):
            min_count = len(step_paths)
            tmp = step_paths

    for file in tmp:
        file_name = os.path.basename(file)
        _key = file_name.split('.')[0]
        frame = cv2.imread(file, 1)
        common_steps.append(StepInfo(_key, frame, subject_count))

    return common_steps


def get_last_timestamp_from_anim_file(file_path):
    with open(file_path, mode='r') as f:
        lines = f.read().splitlines()
        return float(lines[-1].split()[-1])


def generate_steps(path):
    _calib_file = join(path, CALIB_WEBCAM_FILE_NAME)
    _perf_file = join(path, CALIB_PERFORMANCE_FILE_NAME)
    _steps_file = join(path, STEPS_FILE_NAME)
    _video_file = join(path, VIDEO_FILE_NAME)
    _video_timestamp_file = join(path, VIDEO_TIMESTAMP_FILE_NAME)
    _timestamp = 0.0

    # Retrieve last timestamp to avoid detecting steps/stimuli on calibration points
    if exists(_calib_file):
        _timestamp = get_last_timestamp_from_anim_file(_calib_file)
        print('The file "{0}" is not part of the folder : {1}'.format(CALIB_WEBCAM_FILE_NAME, path))

    if exists(_perf_file):
        _timestamp = get_last_timestamp_from_anim_file(_perf_file)
        print('The file "{0}" is not part of the folder : {1}'.format(CALIB_PERFORMANCE_FILE_NAME, path))

    last_timestamp = _timestamp / 1000
    _steps = auto_slice_into_steps(_video_file, _video_timestamp_file, last_timestamp)
    dump_steps(_steps_file, _steps)

    return _steps


def compare_steps(dir, common_steps, logs_info, min_score=0.95, width=1920, height=1080):

    for step in common_steps:
        """
        For each step, search corresponding step(s) for each acquisition and loads fixation file(s)
        # Example :
        # Common step = step 1
        # Corresponding step for subject 0008 = step 6
        Load fixations file then save under step_1_0008
        If more than one step match, fuse the content of each fixations file into a single file
        """
        print(step.name)
        gray_ref = cv2.cvtColor(step.frame, cv2.COLOR_BGR2GRAY)

        for log_dir, step_paths, fix_files in logs_info:

            for _p in step_paths:
                gray = cv2.cvtColor(cv2.imread(_p, 1), cv2.COLOR_BGR2GRAY)
                score, diff = compare_ssim(gray, gray_ref, full=True)

                if score > min_score:
                    step_name = ntpath.basename(_p).split('.')[0]

                    for f in fix_files:
                        if f.startswith(step_name):  # Retrieve fixation file from corresponding step file name
                            print('{0} {1}'.format(f, score))

                            subject = f.split('_')[-1].strip(FIXATIONS_FILE_EXTENSION)

                            # more than 1 fixation file for a step -> merge BEFORE renaming based on step name
                            with open(join(log_dir, f), mode='r') as _f:
                                if subject in step.fixations_by_user.keys():
                                    print('extend')
                                    step.fixations_by_user[subject].extend(_f.readlines())
                                else:
                                    step.fixations_by_user[subject] = _f.readlines()

        if not step.is_valid():
            raise Exception('Not all subjects have fixations for the step: {0}'.format(step.name))

        # Save step
        im = Image.fromarray(cv2.cvtColor(step.frame, cv2.COLOR_BGR2RGB))
        im.save(join(dir, '/{0}.png'.format(step.name)))

        # Save subject fixations for the step in a single file
        for key in step.fixations_by_user:
            _file_name = join(dir, '{0}_{1}{2}'.format(step.name, key, FIXATIONS_FILE_EXTENSION))
            with open(_file_name, mode='w') as _f:
                _f.writelines(step.fixations_by_user[key])

        # Generates FM and SM
        tobii_template = 'tobii'
        group_template = step.name + '_' + tobii_template
        map_path = join(dir, group_template)
        data_t = []

        for key in step.fixations_by_user:
            for line in step.fixations_by_user[key]:
                _splits = line.split()
                data_t.append((round(float(_splits[0])), round(float(_splits[1]))))

        generate_fixations_map(data_t, map_path, width, height)
        generate_saliency_map(data_t, map_path, width, height)


if __name__ == '__main__':

    """
    Detects steps directly from videos then check similarity to find common steps between every acquisition logs.
    Takes a lot of time because it compare video frame-by-frame
    """
    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    _config = read_config(_config_file)
    dir = _config.acquisition_logs_dir

    list_logs_dir = get_acquisition_logs_from_group_dir(dir)
    id = 1  # use to identify log file
    'WARNING: the ids are shared between all groups due to the method: get_acquisition_logs_from_group_dir'

    for log_dir in list_logs_dir:
        complement = '_{:02d}'.format(id)
        steps_file = join(log_dir, STEPS_FILE_NAME)

        '''
        Create or read steps information file. This file contains for each step : 
        - the name of the step
        - the interval of time the step is visible in the video (start and end time are both in seconds)
        '''
        if exists(steps_file):
            steps = load_steps_info_file(steps_file)
        else:
            steps = generate_steps(log_dir)

        '''
        Retreive the fixations for each steps based on their interval of time
        '''
        # Tobii
        retrieve_fixations_by_steps(log_dir, join(log_dir, PREFIX_TOBII + FIXATIONS_FILE_EXTENSION), steps,
                                    file_complement=complement)
        
        # other eye tracking solution
        # retrieve_fixations_by_steps(log_dir, join(log_dir, PREFIX_GAZEML + FIXATIONS_FILE_EXTENSION), steps,
        #                             file_complement=complement)

        print('{1} DIR: {0} DONE '.format(log_dir, id))
        id += 1

    ''' Compare steps with each other to determine which ones have been seen by every subjects '''

    ''' Get the data for each log directory '''
    log_info = [get_log_info(lg_dir) for lg_dir in list_logs_dir]

    hypothetical_common_steps = retrieve_common_step(list_logs_dir)

    compare_steps(dir, hypothetical_common_steps, log_info)
