import os

from datetime import datetime

# Launch multiple python instance to emulate multithreading
# When using this class, be aware of the memory usage the process and, in last resort, 
# reduce the number of threads to be used by heavy process to avoid out of memory exception
class PoolRunner:
    def __init__(self, process):
        self.process = process

    def _process(self, data_by_process):

        self.process(*data_by_process)
        return None

    def run_pool(self, data_by_process, threads=None):
        from multiprocessing import Pool

        _max_threads = os.cpu_count()
        
        if threads is None or threads > _max_threads:
            threads = os.cpu_count() - 1 # the computer is still usable

        _start = datetime.now()

        pool = Pool(processes=threads)
        _result = pool.map(self._process, data_by_process)

        print('Pool ended after: {}'.format(datetime.now() - _start))
