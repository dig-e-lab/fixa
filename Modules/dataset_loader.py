import os
import cv2
import numpy as np

from Models.aoi import from_file
from Modules.file_utils import read_file_content

from common import ANALYSIS_FIXATION_MAPS_DIR, ANALYSIS_FIXATIONS_DIR, ANALYSIS_SALIENCY_MAPS_DIR, \
    ANALYSIS_STIMULI_DIR, ANALYSIS_AOI_DIR

join = os.path.join


def get_stimuli(dataset_path):
    pictures = {}

    stimuli_folder = join(dataset_path, ANALYSIS_STIMULI_DIR)
    files = os.listdir(stimuli_folder)

    for f in files:
        tmp = cv2.imread(join(stimuli_folder, f), 1)
        pictures[os.path.splitext(f)[0]] = tmp

    return pictures


def read_fixation_file(file_path):
    lines = read_file_content(file_path, False)

    fixations = np.zeros((len(lines), 4))
    for i in range(len(fixations)):
        fixations[i] = np.array(lines[i].split()).astype(np.float)

    return fixations


def _read_dataset(dataset_path, stimulus, ref_group):

    fixations_dir = join(dataset_path, ANALYSIS_FIXATIONS_DIR)
    saliency_map_dir = join(dataset_path, ANALYSIS_SALIENCY_MAPS_DIR, stimulus)
    fixation_map_dir = join(dataset_path, ANALYSIS_FIXATION_MAPS_DIR, stimulus)

    groups = []

    for group in os.listdir(fixations_dir):
        subjects = []
        dir_sc = join(fixations_dir, group)
        subject_files = [fname for fname in os.listdir(dir_sc) if fname.startswith(stimulus)]

        for sf in subject_files:
            fixations = read_fixation_file(join(dir_sc, sf))
            subjects.append(Subject(os.path.splitext(sf)[0], fixations))

        sm_path = 'No saliency map'
        for r, d, f in os.walk(saliency_map_dir):
            for file in f:
                if file.startswith(group):
                    sm_path = join(saliency_map_dir, file)
                    break

        fm_path = 'No fixation map'
        for r, d, f in os.walk(fixation_map_dir):
            for file in f:
                if file.startswith(group):
                    fm_path = join(fixation_map_dir, file)
                    break

        gr = Group(group, subjects, sm_path, fm_path)
        gr.ref = group == ref_group
        groups.append(gr)

    return groups


def get_stimulus_areas(dataset_path, stimulus_name):
    areas_img_path = join(dataset_path, ANALYSIS_AOI_DIR, stimulus_name + '.json')

    return from_file(areas_img_path)


def get_data(dataset_path, ref_group):
    """ Macro methods that use all the other methods
    same ref_group for all stimuli
    """

    data = []
    pictures = get_stimuli(dataset_path)

    for step_name in pictures:
        _step_data = _read_dataset(dataset_path, step_name, ref_group)
        stimulus = Stimulus(step_name, pictures[step_name], _step_data)
        stimulus.areas = get_stimulus_areas(dataset_path, stimulus.name)
        data.append(stimulus)

    return data


class Stimulus:
    def __init__(self, name, img, groups):
        self.name = name
        self.image = img
        self.areas = None  # mandatory

        self.groups = groups

    def get_subjects_id(self):
        tmp = []

        for g in self.groups:
            for s in g.subjects:
                tmp.append(s.id)

        return tmp

    def get_subject(self, id):
        for g in self.groups:
            for s in g.subjects:
                if id == s.id:
                    return s

        raise Exception('Subject id was not found')

    def get_bgr_image(self):
        return cv2.cvtColor(self.image, cv2.COLOR_RGB2BGR)

    def set_ref_group(self, id):
        found = False
        for g in self.groups:
            if g.id == id:
                g.ref = True
                found = True
                break

        if not found:
            raise Exception('The stimulus {0} does not have any data from the group with the following id: {1}'
                            .format(self.name, id))


class Group:
    def __init__(self, id, subjects, sm, fm):
        self.id = id
        self.subjects = subjects
        self.ref = False

        self.sm_path = sm
        self.fm_path = fm

        ''' Some metrics are computed using couple of subject (unique)'''
        self.combinations = None

    def get_subject_ids(self):
        tmp = []
        for s in self.subjects:
            tmp.append(s.id)

        return tmp

    def get_subject(self, id):
        for s in self.subjects:
            if id == s.id:
                return s

        raise Exception('Subject id was not found')


class Subject:
    def __init__(self, id, scanpaths):
        self.id = id
        self.fixations = scanpaths
        self.starting_time = self.fixations[0][2]  # beginning of the first fixation
        self.ending_time = self.fixations[-1][3]  # end of the last fixation
