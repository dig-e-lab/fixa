import cv2
import copy
import ntpath
import imagehash

from PIL import Image
from skimage.measure import compare_ssim
from datetime import datetime

from common import *

from Modules.file_utils import read_screen_record_timestamp_file

from Models.step import Step, StepV2, StepHash


SSIM = 'SSIM'
HASH = 'HASH'


def auto_slice_into_steps(video_path, timestamp_file, starting_time, max_score=0.95, min_step_duration=3):

    current_dir = os.path.dirname(video_path)
    timestamps = read_screen_record_timestamp_file(timestamp_file)
    first = timestamps[0]

    # Search step from inside the video
    frame_index, step_index, image_found = -1, 1, False
    gray_last, _actual_step, steps = None, None, []

    def _init_step(index, start_time, frame):
        _tmp = Step('{0}{1}'.format(PREFIX_STEP, index))
        _tmp.start = start_time
        _tmp.frame = frame
        return _tmp

    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():

        def validate_step():
            _actual_step.start = _actual_step.start - first
            _actual_step.end = timestamps[frame_index - 1] - first
            cv2.imwrite(current_dir + '\{0}.png'.format(_actual_step.name), _actual_step.frame)

            steps.append(_actual_step)

        # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            validate_step()
            step_index += 1
            break

        frame_index += 1
        time = timestamps[frame_index]

        if time < starting_time:
            continue

        gray_actual = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        if gray_last is None:
            gray_last = copy.copy(gray_actual)
            continue

        score, diff = compare_ssim(gray_actual, gray_last, full=True)

        if score < max_score:  # new step
            print(score)
            if _actual_step is not None:
                if abs(time - _actual_step.start) > min_step_duration:  # in seconds
                    validate_step()
                    step_index += 1

            _actual_step = _init_step(step_index, time, copy.copy(frame))

        gray_last = copy.copy(gray_actual)

    cap.release()

    return steps


def _process(_tuple):
    score = 0
    image, step = _tuple

    hash = imagehash.phash(image)
    otherhash = imagehash.phash(step.frame)
    score += (hash - otherhash)

    hash = imagehash.dhash(image)
    otherhash = imagehash.dhash(step.frame)
    score += (hash - otherhash)

    hash = imagehash.whash(image)
    otherhash = imagehash.whash(step.frame)
    score += (hash - otherhash)

    return step.name, score


def slice_into_steps(video_path, timestamp_file, image_ref_paths, sim_score=0.9):
    """
    Multi-core step detection

    Depreciated : use Slicer classes instead

    :param video_path:
    :param timestamp_file:
    :param image_ref_paths:
    :param sim_score:
    :return:
    """
    files, steps = [], []

    from multiprocessing import Pool
    pool = Pool()

    timestamps = read_screen_record_timestamp_file(timestamp_file)
    first = timestamps[0]

    for path in image_ref_paths:
        img_name = ntpath.basename(path).split('.')[0]
        static_img = cv2.imread(path, cv2.IMREAD_COLOR)
        _step = StepV2(img_name)
        # _step.frame = cv2.cvtColor(static_img, cv2.COLOR_BGR2GRAY)  # add gray image to object
        _step.frame = Image.fromarray(static_img)
        steps.append(_step)

    frame_count, step_index, image_found = 0, 0, False

    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():  # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            break
        time = timestamps[frame_count]
        gray_video_frame = Image.fromarray(frame) #cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        start = datetime.now()

        couples = [(gray_video_frame, _stp) for _stp in steps]

        result_couples = pool.map(_process, couples)

        for name, score in result_couples:

            print('{0}: {1}'.format(name, score))
            for step in steps:
                if step.name == name:  # ?
                    if score <= 4:  # sim_score
                        if not step.started:
                            step.set_start(time - first)

                    elif step.started:
                        step.set_end_step(time - first)

        print('Video frame number : {0} processed in : {1}'.format(frame_count, (datetime.now() - start)))
        frame_count += 1

    cap.release()

    return steps


def full_screen_video_detection(video_path, timestamp_file, image_ref_start, image_ref_end, score_threshold=0.95):
    """
    Detect a special step that need two images (image_ref_start and image_ref_end)

    :param video_path:
    :param timestamp_file:
    :param image_ref_start: screen shot BEFORE the full screen happened
    :param image_ref_end: screen shot AFTER the full screen happened
    :param score_threshold:
    :return:
    """

    frame_count = -1

    timestamps = read_screen_record_timestamp_file(timestamp_file)
    first = timestamps[0]

    # START
    img_name = ntpath.basename(image_ref_start).split('.')[0]
    static_img = cv2.imread(image_ref_start, cv2.IMREAD_COLOR)
    _step_before_start = Step(img_name)
    _step_before_start.frame = cv2.cvtColor(static_img, cv2.COLOR_BGR2GRAY)

    # END
    img_name = ntpath.basename(image_ref_end).split('.')[0]
    static_img = cv2.imread(image_ref_end, cv2.IMREAD_COLOR)
    _step_before_end = Step(img_name)
    _step_before_end.frame = cv2.cvtColor(static_img, cv2.COLOR_BGR2GRAY)

    _step_corner = _step_before_start
    _video_step = Step('Full_screen_video')

    cap = cv2.VideoCapture(video_path)
    while cap.isOpened():  # Capture frame-by-frame
        ret, frame = cap.read()
        if not ret:
            break

        frame_count += 1
        time = timestamps[frame_count]

        gray_video_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        score = compare_ssim(gray_video_frame, _step_corner.frame)
        print('{0}: {1}'.format(_step_corner.name, score))

        if score >= score_threshold:
            if not _step_corner.started():
                _step_corner.start = (time - first)
                _started = datetime.now()

            if _video_step.start > 0 and time - _video_step.start > 3000:  # already set and more than 3 seconds
                """Video has already started + frame match with step ref. => end of video"""
                _video_step.end = (time - first)
                break

        elif _step_corner.started():
            _step_corner.end = (time - first)
            _step_corner = _step_before_end
            # + start full screen step log
            _video_step.start = (time - first)

    cap.release()

    return _video_step


def get_slicer_based_on_mode(mode, image_ref_paths, thresholds):
    if mode == SSIM:
        return SlicerSSIM(image_ref_paths, thresholds)

    if mode == HASH:
        return SlicerHash(image_ref_paths, thresholds)


class SequentialSlicer:

    def __init__(self, image_ref_paths, thresholds):

        self.steps = self._load_images(image_ref_paths, thresholds)

    def _load_images(self, image_ref_paths, thresholds):
        raise Exception('Not implemented')

    def _process(self, video_frame, actual_step):
        raise Exception('Not implemented')

    def _reset(self):

        for _step in self.steps:
            _step.start = 0.0
            _step.end = 0.0

    def run(self, video_path, timestamp_file, threshold=None, debug=False):
        self._reset()

        frame_count, step_index = 0, 0
        timestamps = read_screen_record_timestamp_file(timestamp_file)
        first = timestamps[0]
        actual = self.steps[step_index]
        thresh = actual.threshold if threshold is None else threshold
        ''' threshold is used over the specific threshold of the step for testing purpose
        it's very difficult to detect every step with only one threshold
        '''

        if thresh is None:
            raise Exception('A threshold is needed to compare the frames and the pictures')

        cap = cv2.VideoCapture(video_path)
        while cap.isOpened():
            ret, frame = cap.read()
            if not ret:
                break

            time = timestamps[frame_count]
            score = self._process(frame, actual)

            if debug and score < 50 :
                print('{0}: {1} - Frame number : {2} processed'.format(actual.name, score, frame_count))

            if score <= thresh:
                if not actual.started():
                    actual.start = (time - first)

            elif actual.started():
                actual.end = (time - first)
                step_index += 1
                if step_index == len(self.steps):
                    break
                else:
                    actual = self.steps[step_index]
                    thresh = actual.threshold if threshold is None else threshold

            frame_count += 1

        cap.release()

        return self.steps


class SlicerHash(SequentialSlicer):

    def _load_images(self, pic_ref_paths, thresholds):
        """
        :param pic_ref_paths: list of the pictures path
        :param thresholds: list of threshold/minimum score for each picture
        :return:
        """

        if len(pic_ref_paths) != len(thresholds):
            raise Exception('Mismatch between step count and threshold count. {0} {1}'.format(len(pic_ref_paths), len(thresholds)))

        _tmp = []
        for path, thresh in zip(pic_ref_paths, thresholds):
            img_name = ntpath.basename(path).split('.')[0]
            _step = StepHash(img_name)
            _step.frame = Image.open(path)
            _step.average_hash = imagehash.average_hash(_step.frame)
            _step.phash = imagehash.phash(_step.frame)
            _step.dhash = imagehash.dhash(_step.frame)
            _step.threshold = thresh
            _tmp.append(_step)

        return _tmp

    def _process(self, video_frame, actual_step):
        score = 0

        video_frame = Image.fromarray(video_frame)  # transform

        """average_hash"""
        hash = imagehash.average_hash(video_frame)
        # otherhash = imagehash.average_hash(actual_step.frame)
        score += (hash - actual_step.average_hash)

        """phash"""
        hash = imagehash.phash(video_frame)
        # otherhash = imagehash.phash(actual_step.frame)
        score += (hash - actual_step.phash)

        """dhash"""
        hash = imagehash.dhash(video_frame)
        # otherhash = imagehash.dhash(actual_step.frame)
        score += (hash - actual_step.dhash)

        """whash (slower)"""
        # hash = imagehash.whash(video_frame)
        # otherhash = imagehash.whash(actual)
        # score += (hash - otherhash)

        return score


class SlicerSSIM(SequentialSlicer):

    def _load_images(self, image_ref_paths, thresholds):
        """ generate steps """
        _tmp = []
        for path in image_ref_paths:
            img_name = ntpath.basename(path).split('.')[0]
            _step = Step(img_name)

            static_img = cv2.imread(path, cv2.IMREAD_COLOR)
            _step.frame = cv2.cvtColor(static_img, cv2.COLOR_BGR2GRAY)  # add gray image to object

            _tmp.append(_step)

        return _tmp

    def _process(self, video_frame, actual_step):

        gray_video_frame = cv2.cvtColor(video_frame, cv2.COLOR_BGR2GRAY)  # transform

        score = compare_ssim(gray_video_frame, actual_step.frame)
        return (1 - score) * 100
