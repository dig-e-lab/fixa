import cv2
import os

# from common import *


# def get_gaze_point_by_devices(gaze_point_files):
#     list_points = []
#
#     for file in gaze_point_files:
#         with open(file, mode='r') as f:
#             _tmp = []
#             for line in f:
#                 values = line.strip().split(' ')
#                 x_y = (float(values[0]), float(values[1]))
#                 _tmp.append(x_y)
#
#             list_points.append((_tmp, get_color()))
#
#     return list_points


def generate_video(video_src_path, video_out_path, list_points):

    cap = cv2.VideoCapture(video_src_path)
    width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = int(cap.get(cv2.CAP_PROP_FPS))

    if os.path.exists(video_out_path):
        print('The following file already exist : {0}'.format(video_out_path))
        return

    out = cv2.VideoWriter(video_out_path, cv2.VideoWriter_fourcc(*"XVID"), fps, (width, height))

    # Check if camera opened successfully
    if not cap.isOpened():
        print("Error opening video stream or file")

    print('Video generation in progress')
    i, r = 1, 10

    # Read until video is completed
    while cap.isOpened():
        # Capture frame-by-frame
        succeed, frame = cap.read()
        if succeed:
            for l, c in list_points:
                x, y = l[i - 1]
                rx = int(round(x))
                ry = int(round(y))
                if rx > 0 and ry > 0:
                    cv2.rectangle(frame, (rx-r, ry-r), (rx + r*2, ry + r*2), thickness=-1, color=c)

            out.write(frame)
        else:
            break

        i = i + 1

    cap.release()
    out.release()
    print('')
