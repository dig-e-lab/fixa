import json

from Modules.utils import ticks_to_timestamp

from Models.config import Config

from common import *


def read_config(path):
    """
    Read configuration file
    :param path:
    :return: Config object
    """
    with open(path) as json_data_file:
        data = json.load(json_data_file)

    _config = Config(data)

    return _config


def is_acquisition_log(dir_path):
    if not os.path.isdir(dir_path):
        return False

    if not os.path.isfile(join(dir_path, VIDEO_FILE_NAME)):
        return False

    if not os.path.isfile(join(dir_path, VIDEO_TIMESTAMP_FILE_NAME)):
        return False

    return True


def get_acquisition_logs_from_group_dir(dir_path):
    """
    :param dir_path: path of the directory that contains the group directories
    :return: acquisition log paths
    """

    _paths = []
    for r, d, f in os.walk(dir_path):  # r=root, d=directories, f=files
        if r is not dir_path:
            break

        for _d in d:
            _paths.extend(get_acquisition_logs_only(join(dir_path, _d)))

    return _paths


def get_acquisition_logs_only(dir_path):
    """
    :param dir_path: path of the directory that contains the acquisition logs
    :return: acquisition log paths
    """
    _all_path = [join(dir_path, _f) for _f in sorted(os.listdir(dir_path))]
    acquisition_log_paths = [path for path in _all_path if is_acquisition_log(path)]

    return acquisition_log_paths


def retrieve_calibration_points(path):
    """
    Isolate calibration point from calibration log
    calibration point = motionless point
    :param path: path of the calibration log
    :return: list of tuple : x, y, start, end
            a tuple by calibration point
    """

    found, first_time, previous, count, data_point = False, None, None, 0, []

    content = read_file_content(path, True)
    tmp = [(float(temp[0]), float(temp[1]), float(temp[2])) for temp in content]  # X Y TIME

    for actual in tmp:
        if previous is None:
            previous = actual
            continue

        previous_x, previous_y, previous_time = previous
        actual_x, actual_y, actual_time = actual

        if previous_x == actual_x and previous_y == actual_y:
            if not found:
                found = True
                first_time = previous_time
            count = count + 1
        elif found:
                if count > 3:  # handle small variation
                    data_point.append(((previous_x, previous_y), first_time, previous_time))

                first_time = None
                count, found = 0, False

        previous = actual

        if tmp.index(actual) == len(tmp)-1:
            if found:  # do not forget the last point
                if count > 2:
                    data_point.append(((previous_x, previous_y), first_time, actual_time))
                    # actual_time because the change could not occurred -> very last log

    return data_point


def find_tobii_file(path, prefix_tobii):
    for file in os.listdir(path):
        if file.startswith(prefix_tobii + '-'):
            return join(path, file)


def convert_tobii_file(path, path_out):
    """
    Create a new file with mandatory information only
    :param path:
    :param path_out:
    """
    # read
    content = read_file_content(path, True)
    # format
    _new_lines = ["{0} {1} {2} {3} {4}".format(data[0], data[1], data[3], data[4], data[-1]) for data in content]
    # write
    write_content_in_file(path_out, _new_lines)


def pupil_sizes_from_file(path):
    content = read_file_content(path, True)

    tmp = [(ticks_to_timestamp(int(data[-1]) * 10), float(data[6]), float(data[7])) for data in content]
    # timestamps, pupil_sizes_left, pupil_sizes_right = zip(*tmp)

    return tmp


def read_screen_record_timestamp_file(path, ticks_format=True):  # Load Logs.txt
    content = read_file_content(path, True, '\t')
    timestamps = [data[-1] for data in content]

    if ticks_format:
        timestamps = [ticks_to_timestamp(int(time)*10) for time in timestamps]

    return timestamps


def read_acquisition_logs(log_tracker):  # read elgLog.log

    """ open log from acquisition software """
    content = read_file_content(log_tracker, True)

    '''timestamp, phi, theta, id_frame, eye_side '''
    data_capture = [(float(data[0]), float(data[1]), float(data[2]), int(data[3]), data[4]) for data in content]

    return data_capture


def get_all_files(dir_path):
    """
    Retrieve all the files contained in the given directory and its sub directories
    """
    list_of_files = os.listdir(dir_path)
    all_files = list()
    # Iterate over all the entries
    for entry in list_of_files:
        # Create full path
        full_path = join(dir_path, entry)
        # If entry is a directory then get the list of files in this directory
        if os.path.isdir(full_path):
            all_files = all_files + get_all_files(full_path)
        else:
            all_files.append(full_path)

    return all_files


def get_files_with_extension(_dir, extensions):
    """

    :param _dir:
    :param extensions: list of extensions
    :return: paths of the file with the right extension
    """
    _files = []
    for r, d, f in os.walk(_dir):  # r=root, d=directories, f=files
        for file in f:
            _files.extend([join(r, file) for _ext in extensions if _ext in file])

    return _files


def file_exist(file):
    _result = exists(file)
    if not _result:
        print('The following file is missing : {0}'.format(file))

    return _result


def read_file_content(path, split=False, char=None):
    """

    :param path:
    :param split: boolean that indicate whenever splitting the line is needed
    :param char: char used for split (if None -> default split char)
    :return: - simple list
         OR  - 2 dimensions list (if split is used)
    """
    file = open(path, 'r')
    content = file.read().splitlines()
    file.close()

    if split:
        content = [line.split(char) if char else line.split() for line in content]

    return content


def write_content_in_file(path_out, content):
    with open(path_out, 'w') as file:
        file.writelines(l + '\n' for l in content)
