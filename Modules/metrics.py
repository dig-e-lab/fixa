import os

import Modules.drawing as drawing

from from_FixaTons.visual_attention_metrics import *

from Models.metricsContainer import MetricsContainer
from Models.aoi import string_edit, get_areas_occupation


def compute_metrics_interval(subject_a, subject_b, areas, start, end):
    """ NOT USED at the moment -> seems irrelevant and, since these metrics use fixations and we are not sure
    to have fixation for the given interval, we need to handle unexpected behaviors"""
    error = False
    met = MetricsContainer()
    fixations_a = subject_a.fixations
    fixations_b = subject_b.fixations

    begin_a, begin_b = fixations_a[0][2], fixations_b[0][2]

    def _by_interval(fixations, start, end):

        _tmp = []
        for _f in fixations:
            if start < _f[2] < end or start < _f[3] < end:
                _tmp.append(_f)

        return _tmp

    tmp_a = _by_interval(fixations_a, begin_a + start, begin_a + end)
    tmp_b = _by_interval(fixations_b, begin_b + start, begin_b + end)

    n1, n2 = len(tmp_a), len(tmp_b)

    if n1 == 0 or n2 == 0:
        error = True

    if n1 > n2:
        tmp_a = tmp_a[:n2]
    elif n1 < n2:
        tmp_b = tmp_b[:n1]

    met.ED = euclidean_distance(tmp_a, tmp_b)

    """ String Edit Distance """
    _string1 = string_edit(fixations_a, areas)
    _string2 = string_edit(fixations_b, areas)
    sed_new = Levenshtein(_string1, _string2)

    d1 = get_areas_occupation(areas, _string1)
    d2 = get_areas_occupation(areas, _string2)

    met.init_occupation(d1, d2)
    met.SED = sed_new

    return error, met


def compute_metrics(subject_a, subject_b, areas):

    fixations_a, fixations_b = subject_a.fixations, subject_b.fixations
    n1, n2 = len(fixations_a), len(fixations_b)
    met = MetricsContainer()

    if n1 > n2:
        fixations_a = fixations_a[:n2]
    elif n1 != n2:
        fixations_b = fixations_b[:n1]

    met.ED = euclidean_distance(fixations_a, fixations_b)

    """ String Edit Distance """
    _string1 = string_edit(fixations_a, areas)
    _string2 = string_edit(fixations_b, areas)
    sed_new = Levenshtein(_string1, _string2)

    d1 = get_areas_occupation(areas, _string1)
    d2 = get_areas_occupation(areas, _string2)

    met.init_occupation(d1, d2)

    met.SED = sed_new
    met.TDED_3_mean = time_delay_embedding_distance(fixations_a, fixations_b, k=3, distance_mode='Mean')
    met.TDED_5_mean = time_delay_embedding_distance(fixations_a, fixations_b, k=5, distance_mode='Mean')
    met.TDED_3_Hausdorff = \
        time_delay_embedding_distance(fixations_a, fixations_b, k=3, distance_mode='Hausdorff')
    met.TDED_5_Hausdorff = \
        time_delay_embedding_distance(fixations_a, fixations_b, k=5, distance_mode='Hausdorff')

    return met


def generate_random(path, shape):
    return drawing.generate_random_fixations_map(path, shape)


def generate_gaussian(path, shape):
    return drawing.generate_gaussian(path, shape)


def compute_metrics_from_maps(g_ref, g, stimulus_name, path, random, gaussian):

    ref_sm = drawing.read_gray_image(g_ref.sm_path)
    ref_fm = drawing.read_gray_image(g_ref.fm_path)
    lambda_sm = drawing.read_gray_image(g.sm_path)

    label1, label2 = g.id + '/' + g_ref.id, g_ref.id + '/' + g_ref.id

    """ KLdiv """
    _KLdiv = KLdiv(lambda_sm, ref_sm)
    # use np.max in order to avoid negative value (pyplot)
    _KLdiv2 = np.max([0, KLdiv(ref_sm, ref_sm)])

    """ NSS """
    _NSS = NSS(lambda_sm, ref_fm)
    _NSS2 = NSS(ref_sm, ref_fm)

    """ AUC """
    points = []
    _AUC_Judd, tp, fp = AUC_Judd_intern(lambda_sm, ref_fm)
    points.append((tp, fp, 'red', label1))
    _AUC_Judd2, tp2, fp2 = AUC_Judd_intern(ref_sm, ref_fm)
    points.append((tp2, fp2, 'green', label2))

    _AUC_Judd_random, tp3, fp3 = AUC_Judd_intern(random, ref_fm)
    points.append((tp3, fp3, 'burlywood', 'random'))

    _AUC_Judd_gaussian, tp4, fp4 = AUC_Judd_intern(gaussian, ref_fm)
    points.append((tp4, fp4, 'b', 'gaussian'))

    AUC_Judd_plot(lambda_sm, ref_fm, _AUC_Judd, points, os.path.join(path, '{0}_AUC_all.png'.format(stimulus_name)))

    values1 = _KLdiv, _NSS, _AUC_Judd
    values2 = _KLdiv2, _NSS2, _AUC_Judd2

    lambda_ref = label1, values1
    ref_ref = label2, values2

    drawing.create_simple_bar_graph(lambda_ref,
                                    ref_ref,
                                    metrics_count=len(values1),
                                    path=os.path.join(path, '{0}_spatial_metrics.png'.format(stimulus_name)))

    print('{0} - done'.format(stimulus_name))

    return lambda_ref, ref_ref
