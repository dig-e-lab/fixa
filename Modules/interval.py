import os
import numpy as np

import from_FixaTons.visual_attention_metrics as metrics
import Modules.drawing as drawing

from Modules.maps_utils import generate_saliency_map, generate_fixations_map


class StimulusByInterval:

    def __init__(self, group_names):
        self.fixations_map_ref = []
        self.saliency_map_ref = []
        self.saliency_maps = {}
        self.intervalsByGroup = {}

        self.m1 = 'KLdiv'
        self.m2 = 'NSS'
        self.m3 = 'AUC'

        # Data groups
        for n in group_names:
            self.saliency_maps[n] = []
            self.intervalsByGroup[n] = []

    def get_shortest_data(self):
        shortest = None
        for k in self.intervalsByGroup:
            actual = self.intervalsByGroup[k]
            if shortest is None or len(shortest) > len(actual):
                shortest = actual

        return shortest

    def get_metrics(self, intervals):
        di = {}

        for k in self.saliency_maps.keys():
            di[k] = {self.m1: [], self.m2: [], self.m3: []}

        for i in range(intervals):
            _fm = self.fixations_map_ref[i]
            _sm = self.saliency_map_ref[i]

            for k in self.saliency_maps.keys():
                s_maps = self.saliency_maps[k]
                data_sm = s_maps[i]

                di[k][self.m1].append(metrics.KLdiv(data_sm, _sm))
                di[k][self.m2].append(metrics.NSS(data_sm, _fm))
                _AUC_Judd, tp, fp = metrics.AUC_Judd_intern(data_sm, _fm, jitter=True)
                di[k][self.m3].append(_AUC_Judd)

        return di


def saliency_by_interval(dir_log, data, interval, expert_group=None, min_subjects=0.6):
    # interval in seconds

    dir_interval = os.path.join(dir_log, 'interval - {0}% of subject'.format(round(min_subjects*100), 1))
    os.makedirs(dir_interval, exist_ok=True)

    data_imgs = {}

    for stimulus in data:
        dir_stim = os.path.join(dir_interval, stimulus.name)
        os.makedirs(dir_stim, exist_ok=True)

        sbi = StimulusByInterval([g.id for g in stimulus.groups])

        for group in stimulus.groups:
            dir_group = os.path.join(dir_stim, group.id)
            os.makedirs(dir_group, exist_ok=True)

            index, remain_fixations, intervals = 0, True, []

            indexes = {}
            for subject in group.subjects:
                indexes[subject.id] = 0

            while remain_fixations:  # one interval at a time

                subject_value = 1.0 / len(group.subjects)
                _value = 1.0  # 100% of the subject remaining

                fixa_per_interval = []
                b_start = interval * index
                b_end = interval * (index+1)

                for subject in group.subjects:

                    start = subject.starting_time + b_start
                    end = subject.starting_time + b_end
                    had_fixation_during_inter = False
                    last_index = indexes[subject.id]
                    _fix = subject.fixations

                    for i in range(last_index, len(_fix)):
                        f = _fix[i]

                        if f[2] < start:
                            continue
                        elif start <= f[2] <= end:
                            fixa_per_interval.append(f)
                            had_fixation_during_inter = True
                        else:
                            indexes[subject.id] = i
                            break

                    if not had_fixation_during_inter:
                        # certify data for a specific amount of subject for each interval
                        _value -= subject_value
                        if _value < min_subjects:
                            fixa_per_interval.clear()
                            break

                if len(fixa_per_interval) == 0:
                    remain_fixations = False
                    sbi.intervalsByGroup[group.id] = intervals
                else:
                    '''create saliency map'''
                    _centers = []
                    height, width, _ = np.shape(stimulus.image)

                    for x, y, t1, t2 in fixa_per_interval:
                        if x < width and y < height:
                            center = (x.round().astype(int), y.round().astype(int))
                            _centers.append(center)

                    _sm_path = os.path.join(dir_group, '{0}_{1}_{2}_{3}'
                                            .format(stimulus.name, group.id, b_start, b_end))
                    _sm = generate_saliency_map(_centers, width, height, file_name=_sm_path)

                    '''Fixations maps reference group'''
                    if group.id == expert_group:
                        _fix_img = generate_fixations_map(_centers, width, height)

                        sbi.fixations_map_ref.append(_fix_img)
                        sbi.saliency_map_ref.append(_sm)

                    sbi.saliency_maps[group.id].append(_sm)
                    intervals.append(b_end)

                index = index + 1

        data_imgs[stimulus.name] = sbi

    if expert_group is not None:
        print('Metrics per interval')
        drawing.plot_analysis_by_intervals(data_imgs, dir_interval, interval)
