import cv2
import os
import numpy as np
import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt
import collections

from PIL import Image


MAX_PIXEL_VALUE = 254


colors = ['blue', 'red', 'olive', 'lightgreen', 'tomato', 'skyblue', 'yellow', 'black', 'grey', 'green', 'pink',
          'maroon', 'cyan', 'blueviolet', 'orange', 'orchid', 'orangered']


def crop_image(img_in, img_out, x1, x2, y1, y2):
    _img = cv2.imread(img_in, cv2.IMREAD_COLOR)  # RGB to BGR

    _crop_img = _img[y1:y2, x1:x2]
    _im = Image.fromarray(cv2.cvtColor(_crop_img, cv2.COLOR_BGR2RGB))
    _im.save(img_out)


def report_fixation_on_image(fixations, width, height):
    # Generate picture
    picture = np.zeros((height, width), np.uint8)

    ignored = 0
    for x, y in fixations:
        if x < width and y < height:
            picture[y, x] = picture[y, x] + 1
        else:
            ignored = ignored + 1

    return picture, ignored


def read_fixation_map(path):
    fixation_map = cv2.imread(path, 1)
    fixation_map[fixation_map > 0] = 1

    return fixation_map


def read_gray_image(path):
    return cv2.imread(path, 0)


def place_fixations(img_2d, centers):

    for x, y in centers:
        img_2d[y][x] = MAX_PIXEL_VALUE

    return img_2d


def gaussian_mask(width, height, sigma=33, center=None, fix=1):
    """
    width  : mask width
    height  : mask height
    sigma  : gaussian Sd
    center : gaussian mean
    fix    : gaussian max
    return gaussian mask
    """
    x = np.arange(0, width, 1, float)
    y = np.arange(0, height, 1, float)
    x, y = np.meshgrid(x, y)

    if center is None:
        x0 = width // 2
        y0 = height // 2
    else:
        if not np.isnan(center[0]) and not np.isnan(center[1]):
            x0 = center[0]
            y0 = center[1]
        else:
            return np.zeros((height, width))

    return fix * np.exp(-4 * np.log(2) * ((x - x0) ** 2 + (y - y0) ** 2) / sigma ** 2)


def apply_gaussian_mask(coordinates, img, width, height, sigma=33):
    sm = np.zeros((height, width), np.float32)
    for coord in coordinates:
        x, y = coord
        sm += gaussian_mask(width, height, sigma=sigma, center=[x, y], fix=img[y][x])

    # Normalization
    sm = sm / np.amax(sm)
    sm = sm * 255
    sm = sm.astype("uint8")

    return sm


def generate_gaussian(path, shape):
    h, w, _ = shape
    gaussian = gaussian_mask(w, h, sigma=300, center=None, fix=MAX_PIXEL_VALUE)

    # Normalization
    gaussian = gaussian / np.amax(gaussian)
    gaussian = gaussian * 255
    gaussian = gaussian.astype('uint8')

    if path is not None:
        save_gray_as_color_image(gaussian, os.path.join(path, 'gaussian_SM.png'))

    return gaussian


def generate_random_fixations_map(path, shape):
    h, w, _ = shape
    _random = np.random.randint(0, 2, (h, w), np.uint8)
    if path is not None:
        tmp = np.array(_random)
        tmp[tmp > 0] = 254
        save_gray_as_color_image(tmp, os.path.join(path, 'random.png'))

    return _random


def save_gray_as_color_image(tmp, path):
    im = Image.fromarray(cv2.cvtColor(tmp, cv2.COLOR_GRAY2RGB))
    im.save(path)


def save_bgr_as_rgb_image(tmp, path):
    im = Image.fromarray(cv2.cvtColor(tmp, cv2.COLOR_BGR2RGB))
    im.save(path)


def save_as(tmp, path):
    im = Image.fromarray(tmp)
    im.save(path)


def scan_path_dump_video(video_out_path, video_src_path, video_timestamp, scan_path, nb_fixations=3,
                         put_lines=True, put_numbers=True, based_on_duration=True):
    if os.path.exists(video_out_path):
        return

    cap = cv2.VideoCapture(video_src_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))
    out = cv2.VideoWriter(video_out_path, cv2.VideoWriter_fourcc(*'XVID'), fps, size)

    with open(video_timestamp, mode='r') as file:
        lines = file.readlines()

    timestamp = []
    ref_time = float(lines[0])
    for l in lines:
        timestamp = float(l) - ref_time

    # pre-process data into a more convenient format
    fixations = []

    for i in range(len(scan_path)):
        fixation = scan_path[i].astype(float)
        x = int(fixation[0])
        y = int(fixation[1])
        duration = fixation[3] - fixation[2]  # milliseconds
        # bellow:  fixation number, x, y start, end, duration
        fixations.append((i + 1, x, y, fixation[2], fixation[3], duration))

    index = 0
    color = (0, 0, 255)
    to_keep = [fixations.pop(0), ]
    while cap.isOpened():
        ret, frame = cap.read()
        _time_frame = timestamp[index]

        frame = np.copy(frame).astype(np.uint8)

        nb, x, y, start, end, dur = to_keep[len(to_keep) - 1]
        if _time_frame > start:
            _fix = fixations.pop(0)

            if len(to_keep) == nb_fixations:  # ensure the maximum fixation to be displayed
                to_keep.pop(0)
            to_keep.append(_fix)

        # Create graphical object for each fixation to display on the frame
        for i in range(len(to_keep)):
            nb, x, y, start, end, dur = to_keep[i]

            cv2.circle(frame, (x, y), 10, color, 3)
            if put_numbers:
                cv2.putText(frame, nb, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), thickness=2)

            if put_lines and i > 0:
                _, pre_x, pre_y, _s, _e, _d = to_keep[i - 1]
                cv2.line(frame, (pre_x, pre_y), (x, y), color, thickness=1, lineType=8, shift=0)

        out.write(frame)

    cap.release()
    out.release()


def scan_path_dump(video_path, image, scan_path, put_lines=True, put_numbers=True, nb_fixations=3, fps=10):

    if os.path.exists(video_path):
        return

    fixations, begin, ref_time = [], True, None

    for i in range(len(scan_path)):

        fixation = scan_path[i].astype(float)
        if begin:
            ref_time = fixation[2]
            # Mandatory in order to map fixations on stimulus
            # (time based on stimulus itself and not on the whole acquisition)
            begin = False

        x = int(round(fixation[0]))
        y = int(round(fixation[1]))
        duration = fixation[3] - fixation[2]  # milliseconds
        # bellow:  fixation number, x, y, start, end, duration
        fixations.append((i + 1, x, y, fixation[2] - ref_time, fixation[3] - ref_time, duration))

    h, w, _ = np.shape(image)
    out = cv2.VideoWriter(video_path, cv2.VideoWriter_fourcc(*"XVID"), fps, (w, h))

    deq = collections.deque(maxlen=nb_fixations)

    # Introduction time to properly see the first fixation
    for j in range(int(fps * 2)):
        out.write(image)  # initial frame

    for i in range(len(fixations)):
        nb, x, y, start, end, duration = fixations[i]

        if i < (len(fixations) - 1):
            _nb, _x, _y, _start, _end, _duration = fixations[i+1]
            next_start = _start
        else:
            next_start = None

        deq.append(fixations[i])

        def customize_image(image, fix_queue):
            color = (0, 0, 255)
            frame = np.copy(image).astype(np.uint8)

            for i in range(len(fix_queue)):
                nb, x, y, start, end, duration = fix_queue[i]
                cv2.circle(frame, (x, y), 10, color, 3)
                if put_numbers:
                    cv2.putText(frame, str(nb), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 0), thickness=2)

                if put_lines and i > 0:
                    _, pre_x, pre_y, _s, _e, _d = fix_queue[i - 1]
                    cv2.line(frame, (pre_x, pre_y), (x, y), color, thickness=1, lineType=8, shift=0)

            return frame

        frame = customize_image(image, deq)
        _tmp = fps * duration

        if next_start is not None:  # take into account the time between the actual fixation and the next fixation
            _tmp += (next_start - end) * fps

        nb_frames = int(round(_tmp))  # narrows round function effect
        for j in range(nb_frames):
            out.write(frame)

    out.release()


def generate_scanpath_video(stimulus, path):
    dir_ex = os.path.join(path, stimulus.name)
    os.makedirs(dir_ex, exist_ok=True)

    for s in stimulus.get_subjects_id():
        scan_path_dump(video_path=os.path.join(dir_ex, '{0}.avi'.format(s.split('.')[0])),
                       image=stimulus.image, scan_path=stimulus.get_subject(s).fixations)


def create_simple_bar_graph(bar1, bar2, metrics_count, path=None, size=(12.8, 7.2), bar_width=0.35, opacity=0.8):
    label1, values1 = bar1
    label2, values2 = bar2

    fig = plt.figure(figsize=size)
    fig.add_subplot()
    index = np.arange(metrics_count)

    plt.bar(index, values1, bar_width,
            alpha=opacity,
            color='steelblue',
            label=label1)

    plt.bar(index + bar_width, values2, bar_width,
            alpha=opacity,
            color='burlywood',
            label=label2)

    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 16,
            }

    plt.xlabel('Metrics', font)
    plt.ylabel('Scores', font)
    plt.xticks(index + (bar_width / 2), ('KLdiv', 'NSS', 'AUC_Judd'))
    plt.legend()

    plt.tight_layout()
    if path is not None:
        plt.savefig(path)

    plt.close(fig)


def generate_graph_comparing_groups(groups_info, group1, group2, path, size=(12.8, 7.2), bar_width=0.35, opacity=0.8):

    metrics_name = ('ED', 'SED', 'TDED_3_mean', 'TDED_5_mean', 'TDED_3_Hausdorff', 'TDED_5_Hausdorff')
    mean_1, stde_1 = groups_info[group1]
    mean_2, stde_2 = groups_info[group2]
    means_neo = mean_1.get_metrics_val()
    means_expert = mean_2.get_metrics_val()

    # create plot
    fig = plt.figure(figsize=size)
    fig.add_subplot()
    index = np.arange(len(metrics_name))

    plt.bar(index, means_neo, bar_width,
            alpha=opacity,
            color='steelblue',
            label=group1)
    y = means_neo
    e = np.array([*stde_1.get_metrics_val()])
    plt.errorbar(index, y, e, linestyle='None', fmt='o', ecolor='red', color='black', capsize=0)

    index_expert = index + bar_width
    plt.bar(index_expert, means_expert, bar_width,
            alpha=opacity,
            color='burlywood',
            label=group2)
    y = means_expert
    e = np.array([*stde_2.get_metrics_val()])
    plt.errorbar(index_expert, y, e, linestyle='None', fmt='o', ecolor='red', color='black', capsize=0)

    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 16,
            }

    plt.xlabel('Metrics', font)
    plt.ylabel('Scores', font)
    plt.title('Scores by group for each metrics')
    plt.xticks(index + (bar_width / 2), metrics_name, rotation=30)

    plt.legend()

    plt.tight_layout()
    plt.savefig(path)
    # plt.show()
    plt.close()


def single_metric_graph(size, xlabel, data, intervals, title, path, special_intervals=None,
                        yticks=None):
    """
    Transforms the data passed as argument as graph then save it as image
    :param size:
    :param xlabel:
    :param data: list of tuple with (id, data)
    :param intervals:
    :param title:
    :param path:
    :param special_intervals:
    :param yticks:
    :return:
    """

    MAX_ITEMS = 4
    data_length = len(data)

    plt.figure(figsize=size)
    plt.xlabel(xlabel)
    for i in range(data_length):
        k, m_data = data[i]
        plt.plot(intervals, m_data, color=colors[i], label=k)

    def draw_bar(p, dy=1, **kw):
        for i, x in enumerate(p):
            plt.plot(np.unique(x), [3 * dy] * len(np.unique(x)),
                     marker="s" * (2 - len(np.unique(x))), ms=kw.get("lw", 2), **kw)

    if special_intervals:
        draw_bar(special_intervals, color="green")

    if yticks is not None:
        plt.yticks(yticks)

    n_col = round(data_length / MAX_ITEMS) if data_length > MAX_ITEMS else 1
    plt.legend(loc='upper left', ncol=n_col)
    plt.title(title)
    plt.savefig(path)
    plt.close()


def plot_analysis_by_intervals(data_imgs, dir_interval, interval, size=(12.8, 7.2)):

    for stim_name in data_imgs.keys():
        sbi = data_imgs[stim_name]
        _intervals = sbi.get_shortest_data()
        container = sbi.get_metrics(len(_intervals))

        dir_stim = os.path.join(dir_interval, stim_name)

        _KLdivs, _NSSs, _AUCs = [], [], []

        # All metrics per group
        for k in container.keys():
            group_metrics = container[k]

            plt.figure(figsize=size)
            plt.xlabel('Time(in seconds)')
            plt.plot(_intervals, group_metrics[sbi.m1], color='lightgreen', label='KL_div')
            plt.plot(_intervals, group_metrics[sbi.m2], marker='o', markerfacecolor=colors[0], markersize=12,
                     color='skyblue', label='NSS')
            plt.plot(_intervals, group_metrics[sbi.m3], marker='D', markerfacecolor=colors[1], markersize=12,
                     color='tomato', label='AUC')
            plt.legend(loc="upper left")
            plt.title(k)  # group name
            plt.savefig(os.path.join(dir_stim, 'All_metrics_interval_{0}_{1}.jpg'.format(k, interval)))
            plt.close()

            _KLdivs.append((k, group_metrics[sbi.m1]))
            _NSSs.append((k, group_metrics[sbi.m2]))
            _AUCs.append((k, group_metrics[sbi.m3]))

        # All group per metrics
        single_metric_graph(size, 'Time(in seconds)', _KLdivs, _intervals, 'KLdiv',
                            os.path.join(dir_stim, 'KL_div_interval_{0}.jpg'.format(interval)))
        single_metric_graph(size, 'Time(in seconds)', _NSSs, _intervals, 'NSS',
                            os.path.join(dir_stim, 'NSS_interval_{0}.jpg'.format(interval)))
        single_metric_graph(size, 'Time(in seconds)', _AUCs, _intervals, 'AUC',
                            os.path.join(dir_stim, 'AUC_interval_{0}.jpg'.format(interval)))
