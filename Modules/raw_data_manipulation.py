from bisect import bisect_left

from Modules.utils import get_screen_ppi, visual_angle_on_screen, INCH
from Modules.file_utils import write_content_in_file

from Models.fixation import process_logs_into_fixations, read_fixation_file
from Models.gazepoint import read_raw_data_file, dummy_log

from common import *


def save_merged_fix(dst, merged_data):
    """
    Save fixations into destination file
    :param dst: destination file
    :param merged_data: fixations (left and right fixation merged together)
    :return:
    """

    if len(merged_data) == 0:
        print('File generation canceled. No data to save({0}).'.format(dst))
        return

    content = ['{0} {1} {2} {3}'.format(fix.x, fix.y, round(fix.start, 4), round(fix.end, 4))
               for fix in merged_data]

    write_content_in_file(dst, content)


def map_raw_data(files_path, video_times):
    """ Return gaze points for each devices used during the acquisition"""
    _devices = []

    for path in files_path:
        tmp_logs = read_raw_data_file(path)
        tmp_times = [log.time for log in tmp_logs]

        _devices.append(([], tmp_times, tmp_logs))

    def find_nearest(value, _to_compare, gaze_points):

        pos = bisect_left(_to_compare, value)

        if pos == 0:
            return dummy_log(time)
        elif pos >= len(gaze_points) - 1:
            return gaze_points[-1]
        else:
            _left = gaze_points[pos]
            _right = gaze_points[pos + 1]

            if abs(value - _left.time) < abs(value - _right.time):
                return _left
            else:
                return _right

    for time in video_times:  # sorted

        for gazes, _t, _logs in _devices:
            gazes.append(find_nearest(time, _t, _logs).get_gp_pixel())

    return [gazes for gazes, _t, _logs in _devices]


def process_raw_into_fixations(path_file, ref_time):
    """
    Synchronize logs and retrieve values from file for to generate the fixations
    """
    opposite_side_cm = visual_angle_on_screen(60)
    pixels_by_cm = get_screen_ppi(23.6) / INCH
    va_pixels = pixels_by_cm * opposite_side_cm  # visual angle pixel

    '''Get fixations (left and right fixation merged)'''
    logs = read_raw_data_file(path_file, to_be_merged=False)

    ''' Synchronizing '''
    index = 0
    for i in range(len(logs)):
        l = logs[i]
        if ref_time < l.time:
            index = i
            break

    sync_logs = logs[index:len(logs)]

    return process_logs_into_fixations(sync_logs, va_pixels)


def retrieve_fixations_by_steps(path, fixation_file, steps, file_complement=''):
    """
    Reads fixation file and create fixation file by steps
    The fixation file need to be generate before-hand
    """

    fixations = read_fixation_file(fixation_file)

    ''' Fixations by steps'''
    last_loop = 0  # only one loop
    for i in range(len(steps)):
        #  Get fixations for a give interval of time (stimulus duration)
        _stp = steps[i]

        for j in range(last_loop, len(fixations)):
            fix = fixations[j]
            fix_start_sec = fix.start  # in seconds
            if _stp.start < fix_start_sec <= _stp.end:
                _stp.logs.append(fix)
            elif fix_start_sec > _stp.end:
                last_loop = j
                break

        save_merged_fix(join(path, '{0}{1}{2}'.format(_stp.name, file_complement, FIXATIONS_FILE_EXTENSION)), _stp.logs)
