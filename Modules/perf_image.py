import numpy as np
import cv2

from PIL import Image

from Models.gazepoint import read_raw_data_file

from Modules.file_utils import retrieve_calibration_points

from common import *

WHITE = 255, 255, 255
BLUE = 255, 0, 0
GREEN = 0, 255, 0
RED = 0, 0, 255


def draw_logs(img, data_anim, logs, color):

    for x_y, start, end in data_anim:
        start, end = start / 1000, end / 1000
        for l_merged in logs:
            if l_merged.is_valid() and start <= l_merged.time <= end:
                gp = l_merged.get_gp_pixel()
                x, y = int(gp.x), int(gp.y)  # round can give max_height and max_width value -> index error
                img[y][x] = color


def generate_performance_image(animation_file, files, max_width=1920, max_height=1080):

    dir_path = os.path.dirname(os.path.realpath(animation_file))
    data_animation9 = retrieve_calibration_points(animation_file)

    img = np.zeros((max_height, max_width, 3), np.uint8)
    circle_radius = 20

    for x_y, start, end in data_animation9:
        x, y = x_y
        circle_position = (int(x * max_width), int(y * max_height))
        # round can give max_height and max_width value -> index error
        cv2.circle(img, circle_position, circle_radius, WHITE, thickness=3, lineType=8, shift=0)

    for _f, color in files:
        draw_logs(img, data_animation9, read_raw_data_file(_f), color)

    im = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    im.save(dir_path + '/' + CALIB_PERFORMANCE_IMAGE)


def generate_performance_image_from_dir(log_dir):

    _to_test = join(log_dir, CALIB_PERFORMANCE_IMAGE)
    if exists(_to_test):
        print('The following file already exist : {0}'.format(_to_test))
        return

    _mandatory_file = join(log_dir, CALIB_PERFORMANCE_FILE_NAME)
    if not exists(_mandatory_file):
        print('Could not generate performance image. The following file was missing : {0}'
              .format(_mandatory_file))
        return

    tobii_file = join(log_dir, TOBII_LOGS_FILE_NAME)

    to_read = []

    if exists(tobii_file):
        to_read.append((tobii_file, GREEN))

    gazeml_file = join(log_dir, CARTESIAN_LOGS_FILE_NAME)
    if exists(gazeml_file):
        to_read.append((gazeml_file, BLUE))

    if len(to_read) > 0:
        generate_performance_image(_mandatory_file, to_read)
    else:
        print('Unable to generate a performance image because no gaze data were found in the following folder : {0}')


if __name__ == '__main__':

    log_dir = '' # CSR log directory

    generate_performance_image_from_dir(log_dir)



