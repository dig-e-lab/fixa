import cv2

def get_heatmap_overlay(img, saliency_map, alpha=0.5, beta=0.5):
    heatmap_img = cv2.applyColorMap(saliency_map, cv2.COLORMAP_JET)

    overlay = cv2.addWeighted(heatmap_img, alpha, img, beta, 0)
    return overlay


def display(frame):
    cv2.imshow('image', frame)
    cv2.waitKey()


if __name__ == '__main__':
    img_path = ''
    gaussian = ''
    img = cv2.imread(img_path, 1)
    display(img)

    gray_img = cv2.imread(gaussian, 0)
    # display(gray_img)
    #
    # heatmap_img = cv2.applyColorMap(gray_img, cv2.COLORMAP_JET)
    # display(heatmap_img)

    fin = get_heatmap_overlay(img, gray_img)
    display(fin)
