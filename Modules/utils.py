from numpy import mean
from datetime import datetime
from math import factorial, tan, pi, sqrt

INCH = 2.54

WHITE = 255, 255, 255
RED = 0, 0, 255
BLUE = 255, 0, 0
GREEN = 0, 255, 0
YELLOW = 0, 255, 255

colors = [GREEN, BLUE, YELLOW, RED]


def get_color(index):
    global colors

    tmp_color = colors[index]
    return tmp_color


def simple_list_from_2dmatrix(matrix, to_do):
    """
    Uses list expression to return a list of object from 2-dimensional matrix
    :param matrix: List[List[Any]]
    :param to_do: function to perform
    :return: List[Any] where Any is the return value of the to_do method
    """
    return [to_do(data) for row in matrix for data in row]


def list_of_means(_2d_lists):
    """ Compute the mean between every elements of each list
    :param _2d_lists: 2 dimensions array
    """

    '''Find the shortest list'''
    shortest_count = min([len(_list) for _list in _2d_lists])

    if shortest_count == 0:
        raise Exception('At least one array was empty. To be checked.')

    '''Resize all lists so they are of the same length'''
    _2d_lists = [_l[:shortest_count] for _l in _2d_lists]

    _means = list(mean(_2d_lists, axis=0))

    return _means


def ticks_to_timestamp(ticks):

    epoch_ticks = 621355968000000000
    ticks_per_milliseconds = 10000
    # date_max_milliseconds = 8640000000000000

    ticks_since_epoch = ticks - epoch_ticks
    milliseconds_since_epoch = ticks_since_epoch // ticks_per_milliseconds
    return milliseconds_since_epoch / 1000


def timestamp_to_ticks(ts):
    dt = datetime.utcfromtimestamp(ts)
    return (dt - datetime(1, 1, 1)).total_seconds() * 10 ** 7


def binomial_coefficient(n, k=2):
    return factorial(n) / (factorial(k) * factorial(n - k))


def generate_combinations(items):
    couple_sub = []
    n = len(items)

    for i in range(n):
        sub = items[i]

        for j in range(n):
            if sub != items[j]:
                double = False
                tmp_count = len(couple_sub)

                if tmp_count > 0:
                    for z in range(tmp_count):
                        s1, s2 = couple_sub[z]
                        if s1 == items[j] and s2 == sub:
                            double = True
                            break

                    if not double:
                        couple_sub.append((sub, items[j]))
                else:
                    couple_sub.append((sub, items[j]))

    if len(couple_sub) != binomial_coefficient(n):
        raise Exception('Algorithm error')

    return couple_sub


def get_screen_ppi(diagonal_inch, width=1920, height=1080, aspect_ratio=(16, 9)):
    #  reference : https://www.sven.de/dpi/
    ratio_a, ratio_b = aspect_ratio
    aspect_r_hypotenuse = sqrt((ratio_a*ratio_a) + (ratio_b*ratio_b))

    # Hypotenuse percentage calculation
    w_hypotenuse = ratio_a / aspect_r_hypotenuse
    h_hypotenuse = ratio_b / aspect_r_hypotenuse

    w_inch = w_hypotenuse * diagonal_inch
    h_inch = h_hypotenuse * diagonal_inch

    surface_inch = w_inch * h_inch

    return sqrt((width * height) / surface_inch)


def visual_angle_on_screen(distance_from_screen=60, fovea_angle=2):
    radiant = fovea_angle * (pi/180)
    opposite_side = tan(radiant/2) * distance_from_screen
    return opposite_side * 2  # diameter expressed in centimeters


if __name__ == '__main__':

    opposite_side_cm = visual_angle_on_screen(60)
    print(opposite_side_cm)

    ppi = get_screen_ppi(10)
    print(ppi)
    ppi = get_screen_ppi(27)
    print(ppi)
    ppi = get_screen_ppi(50)
    print(ppi)

    ppi = get_screen_ppi(23.6)
    print(ppi)
    print('Pixels by centimeter: {0}'.format(ppi/INCH))
