from common import *

from Modules.file_utils import read_file_content, write_content_in_file, read_screen_record_timestamp_file
from Modules.pyHampel import hampel, np
from Modules.raw_data_manipulation import map_raw_data
from Modules.utils import get_color
from Modules.video_gaze import generate_video


def apply_hampel_filter(data, k=20, mode='center'):
    items = np.array(data)
    _new, _bool = hampel(items, k, mode)  # need np.array

    return _new


if __name__ == '__main__':

    _dir = ''

    checkDir(_dir) # CSR log directory

    data = read_file_content(join(_dir, CARTESIAN_LOGS_FILE_NAME), True)
    ks = [25, 30]

    tmp = [(float(args[0]), float(args[1]), float(args[2]), float(args[3]), int(args[4]))
           for args in data]
    xl, yl, xr, yr, time = zip(*tmp)

    for k in ks:  # test different parameters

        file_filtered_name = 'Logs_smooth_{0}.log'.format(k)

        def _filter(data):
            return apply_hampel_filter(data, k)


        tmp = zip(_filter(xl), _filter(yl), _filter(xr), _filter(yr), time)

        content = ['{0} {1} {2} {3} {4}'.format(float(xl), float(yl), float(xr), float(yr), t)
                   for xl, yl, xr, yr, t in tmp]
        write_content_in_file(join(_dir, file_filtered_name), content)

        ''' Generate gazepoint video'''
        list_points = []
        video_name = join(_dir, 'Video_filter_{0}.avi'.format(k))
        video_times = read_screen_record_timestamp_file(join(_dir, VIDEO_TIMESTAMP_FILE_NAME))

        list_gaze_by_devices = map_raw_data([join(_dir, TOBII_LOGS_FILE_NAME),
                                             join(_dir, file_filtered_name)],
                                            video_times)

        for i in range(len(list_gaze_by_devices)):
            list_points.append(([(gp.x, gp.y) for gp in list_gaze_by_devices[i]], get_color(i)))

        generate_video(join(_dir, VIDEO_FILE_NAME), video_name, list_points)
