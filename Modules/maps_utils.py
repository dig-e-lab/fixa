import os
import cv2
import numpy as np

from PIL import Image

from Modules.drawing import report_fixation_on_image, apply_gaussian_mask


def generate_fixations_map(data, width, height, file_name=None):

    # picture, ignored = report_fixation_on_image(data, width, height)

    picture = np.zeros((height, width), np.uint8)

    for x, y in data:
        if x < width and y < height:
            picture[y, x] = 255

    # for y in range(len(picture)):
    #     for x in range(len(picture[y])):
    #         if picture[y][x] > 0:
    #             picture[y][x] = 255

    if file_name is not None:
        im = Image.fromarray(cv2.cvtColor(picture, cv2.COLOR_GRAY2RGB))
        im.save(os.path.join(file_name + '_FM.png'))

    return picture


def generate_saliency_map(data, width, height, sigma=50, file_name=None):

    picture, _ = report_fixation_on_image(data, width, height)

    sm = apply_gaussian_mask(data, picture, width, height, sigma=sigma)

    if file_name is not None:
        im = Image.fromarray(cv2.cvtColor(sm, cv2.COLOR_GRAY2RGB))
        im.save(os.path.join(file_name + '_SM.png'))

    return sm
