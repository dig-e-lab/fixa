import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt
import numpy as np
import logging
import cv2

from copy import copy

from Models.eyesInfo import EyesInfo
from Modules.file_utils import read_acquisition_logs, retrieve_calibration_points, file_exist, write_content_in_file

from common import *


def get_cartesian_coordinates(theta, phi, r=1):  # r = distance user-screen
    # theta was used as vertical axis and phi as horizontal axis
    # so theta and phi are used accordingly

    x = r * np.sin(theta) * np.cos(phi)  # originally: z * np.sin(self.phi) * np.cos(self.theta)
    y = r * np.sin(theta) * np.sin(phi)  # originally: z * np.sin(self.phi) * np.sin(self.theta)
    return x, y


def get_cartesian_coord(calibration_data, debug=False):
    vertices_src = []

    # Cartesian
    tmp_x, tmp_y = [], []

    for centroid in calibration_data:
        x, y = get_cartesian_coordinates(centroid.theta, centroid.phi)

        # Cartesian
        tmp_x.append(x)
        tmp_y.append(y)
        # Cartesian

        vertices_src.append(np.asarray([x, y], dtype='float32'))

    # Cartesian
    if debug:
        plt.plot(tmp_x, tmp_y, 'bs')
        plt.xlabel('x')
        plt.ylabel('y')
        plt.title('Cartesian')
        plt.show()
    # Cartesian

    return vertices_src


def print_list_content_format(_list, list_name):
    print('List content: ' + list_name)
    [print(item) for item in _list]


class CalibrationPoint:
    def __init__(self, theta, phi, center):
        self.theta = theta
        self.phi = phi
        self.pixel_x, self.pixel_y = center

    def get_calibration_point(self):
        return self.pixel_x, self.pixel_y

    def get_cartesian_coord_array(self):
        x, y = get_cartesian_coordinates(self.theta, self.phi)
        return np.asarray([x, y], dtype='float32')

    def __str__(self):
        return 'Content(theta:{0}; phi:{1}; pixel_x:{2}; pixel_y:{3})' \
            .format(self.theta, self.phi, self.pixel_x, self.pixel_y)


class Calibration:
    def __init__(self, animation_corners, data_capture, output_dir):
        self.transformation_matrix = None
        self.data_capture = data_capture

        ''' Destination vertices '''
        self.vertices_dst = [np.asarray([0, 0]), np.asarray([1, 0]), np.asarray([1, 1]), np.asarray([0, 1])]

        ''' init logger '''
        self.logger = logging.getLogger('result_generator_info')  # after calibration(4 corners of the screen)
        f_handler = logging.FileHandler(join(output_dir, self.logger.name + '.log'), mode='w')
        self.logger.addHandler(f_handler)
        self.logger.setLevel(logging.INFO)

        self._compute_transform_matrix(animation_corners)

    def _get_calibration_data(self, data_animation, debug=False, show_graph_by_calibration_point=False):

        calibration_data, list_global, theta_phi, centroids = [], [], [], []

        for fixation_info in data_animation:
            center, begin, end = fixation_info

            for acquisition in self.data_capture:
                timestamp, phi, theta, id_frame, eye_side = acquisition

                if begin < timestamp < end:

                    theta_phi.append((theta, phi))
                    self.logger.info(str(theta) + ' ' + str(phi))

                elif end < timestamp:

                    list_global.append(copy(theta_phi))
                    list_theta, list_phi = zip(*theta_phi)

                    c_phi = np.mean(list_phi)
                    c_theta = np.mean(list_theta)
                    centroids.append((c_theta, c_phi))

                    calibration_data.append(CalibrationPoint(c_theta, c_phi, center))

                    if show_graph_by_calibration_point:
                        plt.plot(list_theta, list_phi, 'ro', c_theta, c_phi, 'bs')
                        plt.axis([-np.pi, np.pi, -np.pi, np.pi])
                        plt.xlabel('theta')
                        plt.ylabel('phi')
                        plt.title(str(len(list_global)))
                        plt.show()

                    theta_phi.clear()  # reset
                    break

        if debug:
            for item in list_global:
                list_theta, list_phi = zip(*item)
                plt.plot(copy(list_theta), copy(list_phi), 'ro')

            c_x, c_y = zip(*centroids)
            plt.plot(c_x, c_y, 'bs')
            plt.axis([-np.pi, np.pi, -np.pi, np.pi])
            plt.xlabel('theta')
            plt.ylabel('phi')
            plt.title('General')
            plt.show()

        return calibration_data

    def _compute_transform_matrix(self, _corners):
        vertices_src = get_cartesian_coord(self._get_calibration_data(_corners))

        if len(vertices_src) < 4:
            raise Exception('There is an issue with the calibration data. '
                            'Not enough vertices to calculate the transformation matrix. '
                            'Number of vertices from calibration data: {0} '.format(len(vertices_src)))

        self.transformation_matrix = cv2.getPerspectiveTransform(np.asarray(vertices_src),
                                                                 np.asarray(self.vertices_dst, dtype='float32'))

    def log_gaze_points(self, output_path, first_time):

        actual_eyes = None
        count_bad_frame = 0
        eyes = []

        for acquisition in self.data_capture:
            timestamp, phi, theta, id_frame, eye_side = acquisition

            if timestamp < first_time:
                continue

            if actual_eyes is None:
                actual_eyes = EyesInfo(id_frame, timestamp)

            elif id_frame != actual_eyes.id:
                ''' log + count invalid logs '''
                if not actual_eyes.is_valid():
                    self.logger.info(actual_eyes)
                    count_bad_frame = count_bad_frame + 1

                eyes.append(actual_eyes)
                actual_eyes = EyesInfo(id_frame, timestamp)

            x, y = get_cartesian_coordinates(theta, phi)
            tmp = np.asarray([x, y], dtype='float32')
            result = cv2.perspectiveTransform(np.asarray([[tmp]], dtype='float32'), self.transformation_matrix)

            x, y = result[0][0][0], result[0][0][1]

            if eye_side == 'left':
                actual_eyes.left.x = x
                actual_eyes.left.y = y
            elif eye_side == 'right':
                actual_eyes.right.x = x
                actual_eyes.right.y = y

        write_content_in_file(output_path, [str(e) for e in eyes])

        self.logger.info(
            'Error rate: {0}%'
            .format(round((count_bad_frame/len(self.data_capture))*100)))
        self.logger.info('Logs that is missing a eye or from which the gaze could not be placed on screen(negative x/y)')

    def get_gaze_point_on_screen(self, data_animation, output_path, debug=False):

        calibration_data = self._get_calibration_data(data_animation, debug)
        vertices_src = get_cartesian_coord(calibration_data, debug)

        result = cv2.perspectiveTransform(np.asarray([vertices_src]), self.transformation_matrix)

        centroids, to_dump = [], []
        for i in range(len(result)):
            for j in range(len(result[i])):
                x = result[i][j][0]
                y = result[i][j][1]
                centroids.append((x, y))
                to_dump.append('{0} | {1} original: {2}'.format(x, y, calibration_data[j].get_calibration_point()))

        write_content_in_file(output_path, to_dump)

        if debug:
            c_x, c_y = zip(*centroids)
            plt.plot(c_x, c_y, 'bs')
            plt.xlabel('x')
            plt.ylabel('y')
            plt.title('Cartesian result')
            plt.show()

    def close(self):
        for hdlr in self.logger.handlers:
            self.logger.removeHandler(hdlr)


def main(log_dir):
    """"
    Only used for open-source eye tracker that do not convert their data into cartesian coordinates
    :param log_dir:
    :return:
    """
    _webcam_file = join(log_dir, CALIB_WEBCAM_FILE_NAME)
    _angle_raw_data = join(log_dir, GAZEML_LOGS_FILE_NAME)
    _cartesian_path = join(log_dir, CARTESIAN_LOGS_FILE_NAME)
    _anim9_path = join(log_dir, CALIB_PERFORMANCE_FILE_NAME)
    _perf_path = join(log_dir, "result_performance.txt")

    if not file_exist(_webcam_file) or not file_exist(_angle_raw_data):
        return

    if exists(_cartesian_path):
        print('The following file already exist : {0}'.format(_cartesian_path))
        return

    data_animation4 = retrieve_calibration_points(_webcam_file)
    data_acquisition = read_acquisition_logs(_angle_raw_data)

    calibration_mod = Calibration(data_animation4, data_acquisition, log_dir)

    if exists(_anim9_path):
        data_animation9 = retrieve_calibration_points(_anim9_path)
        calibration_mod.get_gaze_point_on_screen(data_animation9, _perf_path)

    _c, _t, time = data_animation4[-1]  # last
    calibration_mod.log_gaze_points(_cartesian_path, first_time=time)
    calibration_mod.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Demonstration of landmarks localization.')
    parser.add_argument('--dir', type=str, help='Logs path')
    args = parser.parse_args()

    path = str(args.dir) # CSR log directory

    checkDir(path)
    main(path)
