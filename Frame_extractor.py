import cv2
import numpy as np

from common import *

if __name__ == '__main__':

    """
    Use this script to extract frames from a given video using video timestamps (in seconds)
    """

    parser = argparse.ArgumentParser(description='Demonstration of landmarks localization.')
    parser.add_argument('--dir', type=str, help='Logs path')
    parser.add_argument('--nargs', type=int, nargs='+')
    args = parser.parse_args()

    logs_dir = args.dir
    
    checkDir(logs_dir)

    _times = args.nargs# [110, 220 , ] # seconds

    if _times is None or len(_times) == 0:
        print('No times passed as argument')
    else:
        _times = [_t * 1000 for _t in sorted(_times)]
        print(_times)

    v_in = os.path.join(logs_dir, VIDEO_FILE_NAME)
    print(v_in)

    cap = cv2.VideoCapture(v_in)
    fps = cap.get(cv2.CAP_PROP_FPS)
    size = (int(cap.get(cv2.CAP_PROP_FRAME_WIDTH)), int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT)))

    _interval = 1000/fps  # interval between two frames

    # Check if camera opened successfully
    if not cap.isOpened():
        print("Error opening video stream or file")

    _index_time, frame_count = 0, 1

    # Read until video is completed
    while cap.isOpened():
        # Capture frame-by-frame
        ret, frame = cap.read()
        print("frame: {0} ret: {1}".format(frame_count, ret))
        if ret:
            _actual_time = frame_count * _interval

            if _actual_time >= _times[_index_time]:
                cv2.imwrite(os.path.join(logs_dir, 'step_{0:03d}.png'.format((_index_time+1))), frame)

                _index_time += 1
                if _index_time > len(_times) - 1:
                    break

            # Press Q on keyboard to exit
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        # Break the loop
        else:
            break

        frame_count = frame_count + 1

    # When everything done, release the video capture object
    cap.release()

    # Closes all the frames
    cv2.destroyAllWindows()
