import os
import argparse

join = os.path.join
exists = os.path.exists

VIDEO_FILE_NAME = 'Logs.avi'
VIDEO_TIMESTAMP_FILE_NAME = 'Logs.txt'

CARTESIAN_LOGS_FILE_NAME = 'Logs.log'

GAZEML_LOGS_FILE_NAME = 'elgLog.log'

OPENGAZE_ORIGINAL_FILE_NAME = 'CamRecord_gaze_output.txt'
OPENGAZE_FILE_NAME = 'opengaze.log'

WEBCAM_FILE_NAME = 'CamRecord.avi'
WEBCAM_TIMESTAMP_FILE_NAME = 'CamRecord.txt'

CALIB_WEBCAM_FILE_NAME = 'anim4.log'
CALIB_PERFORMANCE_FILE_NAME = 'anim9.log'
CALIB_PERFORMANCE_IMAGE = 'calibration_performance.png'

TOBII_LOGS_FILE_NAME = 'Tobii.log'

PREFIX_TOBII = 'Logs_X230C'
PREFIX_GAZEML = 'Logs'
PREFIX_STEP = 'Step_'

STEPS_FILE_EXTENSION = '.json'
STEPS_FILE_NAME = 'steps' + STEPS_FILE_EXTENSION
STEPS_TEXT_FILE_NAME = 'Steps.txt'

FIXATIONS_FILE_EXTENSION = '.fix'

VIDEO_GAZE_POINTS_FILE = 'Logs_gaze_points.avi'

ANALYSIS_STIMULI_DIR = 'STIMULI'
ANALYSIS_AOI_DIR = 'AOI'
ANALYSIS_FIXATIONS_DIR = 'FIXATIONS'
ANALYSIS_FIXATION_MAPS_DIR = 'FIXATION_MAPS'
ANALYSIS_SALIENCY_MAPS_DIR = 'SALIENCY_MAPS'


def ProcessConfigFileArgs():
    parser = argparse.ArgumentParser(description='Demonstration of landmarks localization.')
    parser.add_argument('--config_file', type=str, help='Configuration file ')
    args = parser.parse_args()

    return args.config_file


def checkDir(path):
    if not path:
        raise Exception('The path is not defined.')

    assert os.path.isdir(path)