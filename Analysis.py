import numpy as np

from common import *

import Modules.drawing as drawing

from Models.metricsContainer import MeanContainer, StandardErrorContainer

from Modules.dataset_loader import get_data, os
from Modules.metrics import compute_metrics, compute_metrics_from_maps, generate_gaussian, generate_random
from Modules.interval import saliency_by_interval
from Modules.utils import generate_combinations, list_of_means
from Modules.file_utils import read_config
from Modules.poolRunner import PoolRunner

from Models.aoi import get_areas_text_default_values, show_areas_on_image, AOI


def opacity_map(sm_path, step_name, step_img, group_name, dest_path):
    _out = join(dest_path, '{0}_{1}_opacity_map.png'.format(step_name, group_name))
    _fixations = drawing.read_fixation_map(sm_path)
    _opacity = step_img * _fixations

    drawing.save_bgr_as_rgb_image(_opacity, _out)


def compute_metrics_from_fixations(working_dir, stimulus):
    _ed_per_group, groups_info = [], {}
    pic = stimulus.get_bgr_image()
    areas = stimulus.areas

    _crossing_data = len(stimulus.groups) > 1

    '''Display areas on stimulus'''
    show_areas_on_image(pic, areas, get_areas_text_default_values(areas),
                        join(working_dir, 'areas_{0}.png'.format(stimulus.name)))

    for group in stimulus.groups:

        if group.combinations is None:
            group.combinations = generate_combinations(group.get_subject_ids())

        '''Process metrics by couple of subject'''
        metrics_data = [compute_metrics(group.get_subject(s1), group.get_subject(s2), stimulus.areas)
                        for s1, s2 in group.combinations]

        metrics_mean = MeanContainer(metrics_data)
        if _crossing_data:
            standard_error = StandardErrorContainer(metrics_data)
            '''Mandatory in order to compare groups with each other'''
            groups_info[group.id] = (metrics_mean, standard_error)

        '''AOI occupation per group'''
        occupation_img_path = join(working_dir, 'occupation_{0}_{1}.png'.format(stimulus.name, group.id))
        show_areas_on_image(pic, stimulus.areas, metrics_mean.occupation_by_zone, occupation_img_path)

        '''Collecting euclidean distance data'''
        _ed = list_of_means([m.ED for m in metrics_data])
        _ed_per_group.append((group.id, _ed))

        _mean = np.mean(_ed)
        _ed_mean = np.zeros(len(_ed))
        _ed_mean[:] = _mean
        _ed_per_group.append(('{0}_mean'.format(group.id), _ed_mean))

    '''ED graph'''
    x_length = min([len(l) for k, l in _ed_per_group])
    if x_length > 5:  # less than 5 will not give enough data for interpretation
        _ed_per_group = [(k, l[:x_length]) for k, l in _ed_per_group]
        drawing.single_metric_graph((12.8, 7.2), 'Fixations',
                                    _ed_per_group, range(x_length), 'Euclidean distance',
                                    path=join(working_dir, 'ED_fixations_{0}.jpg'.format(stimulus.name)))

    if _crossing_data:
        '''Graph that compare groups'''
        graph_path = join(working_dir, '{0}_time_based_metrics.png'.format(stimulus.name))
        drawing.generate_graph_comparing_groups(groups_info,
                                                group1=stimulus.groups[0].id,
                                                group2=stimulus.groups[1].id,
                                                path=graph_path)

    print('{0} - done'.format(stimulus.name))


def compute_opacity_maps(working_dir, stim):

    [opacity_map(group.sm_path, stim.name, stim.image, group.id, working_dir) for group in stim.groups]


def _run_pool(process, data_by_process, threads=None):
    _pr = PoolRunner(process)
    _pr.run_pool(data_by_process, threads)


def main(working_dir, data,
         reference_group=None,
         opacity_map_needed=True,
         fixation_metrics_needed=True,
         interval_needed=False,
         scanpath_video_needed=False,
         spatial_metrics_needed=True):

    if fixation_metrics_needed:
        _run_pool(compute_metrics_from_fixations,
                  [(working_dir, stimulus) for stimulus in data])

    ''' Spatial metrics on groups: NSS, KLDiv and AUC
        Load all SM/FM and work with them -> spatial metrics
        '''
    if spatial_metrics_needed:
        print('Spatial metrics on groups')

        _shape = np.shape(data[0].image)
        _random = generate_random(working_dir, _shape)
        _gaussian = generate_gaussian(working_dir, _shape)

        _data = []
        for stim in data:
            if len(stim.groups) < 2:
                continue

            if len(stim.groups) > 2:
                raise Exception('Only 2 groups is supported for now')

            g1, g2 = stim.groups[0], stim.groups[1]
            if g1.ref:
                g_ref = g1
                g = g2
            elif g2.ref:
                g_ref = g2
                g = g1
            else:
                raise Exception('No reference group detected.')

            _data.append((g_ref, g, stim.name, working_dir, _random, _gaussian))

        _run_pool(compute_metrics_from_maps, _data)

    if opacity_map_needed:
        print('Generating opacity maps')

        _run_pool(compute_opacity_maps,
                  [(working_dir, stimulus) for stimulus in data])

    if interval_needed:
        print('Saliency maps based on time (fixed interval)')
        saliency_by_interval(working_dir, data, interval=3, expert_group=reference_group)

    if scanpath_video_needed:
        print('Generating scanpath video if they do not exist')
        dir_video = join(working_dir, 'video')
        os.makedirs(dir_video, exist_ok=True)

        _run_pool(drawing.generate_scanpath_video, [(step, dir_video) for step in data], 4)


if __name__ == '__main__':

    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    _config = read_config(_config_file)
    _c_analysis = _config.analysis
    _dataset_path = _config.output_analysis_dir

    ''' Retrieve data from files (database)'''
    _data = get_data(_dataset_path, _c_analysis.ref_group)

    dir_result = join(_dataset_path, 'result')
    os.makedirs(dir_result, exist_ok=True)

    main(dir_result, _data, _c_analysis.ref_group,
         _c_analysis.opacity_map_needed,
         _c_analysis.fixation_metrics_needed,
         _c_analysis.interval_needed,
         _c_analysis.scanpath_video_needed,
         _c_analysis.spatial_metrics_needed)
