import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt

from statistics import stdev

from common import *

from Modules.file_utils import find_tobii_file, get_acquisition_logs_only, pupil_sizes_from_file, \
    read_screen_record_timestamp_file
from Modules.drawing import single_metric_graph, np
from Modules.pyHampel import hampel

from Models.step import load_steps_info_file

EYE_TRACKER_FREQUENCY = 30
TIME_BETWEEN_LOGS = 1000//EYE_TRACKER_FREQUENCY

mean = np.mean


class PupilInfo:
    def __init__(self):
        self.logs = []
        self.mean = None
        self.sd = None  # standard deviation

        # standardization : standard score/ z-score
        # ref. : https://www.statisticshowto.com/probability-and-statistics/z-score/
        # count/number of standard deviation value(sd) from the mean
        self.z_score = None

    def add_logs(self, data):
        self.logs.append(data)

    def is_valid(self):
        return self.mean is not None and self.mean > 0\
               and self.sd is not None

    def validate(self):
        l = self.logs

        if len(l) > 1:
            self.mean = mean(l)
            self.sd = stdev(l)

    def standardization(self, mean, sd):
        self.z_score = [(d - mean)/sd for d in self.logs]

    def __str__(self):
        return 'Mean: {0} standard deviation: {1}'.format(
            self.mean, self.sd)


class StepInfo:
    def __init__(self, id):
        super().__init__()
        self.name = id
        # self.logs_left = []
        # self.logs_right = []

        ''' step mean/ subject mean'''
        self.multi_l = None
        self.multi_r = None

        self.left = PupilInfo()
        self.right = PupilInfo()

        self._mean_both = None

    def is_valid(self):
        return self.left.is_valid() and self.right.is_valid()

    def add_logs(self, left, right):
        self.left.add_logs(left)
        self.right.add_logs(right)

    def validate(self):
        self.left.validate()
        self.right.validate()

        if self.is_valid():
            self._mean_both = (self.left.mean + self.right.mean) / 2

    def __str__(self):
        return '{0} : {1}'.format(self.name, super().__str__())


class PupilMetric:
    def __init__(self):
        """ Diameter """
        self.mean_left = None
        self.mean_right = None

        ''' standard deviation '''
        self.sd_left = None
        self.sd_right = None

        self._mean_both = None

    def _set_both(self):
        if self.mean_left and self.mean_right:
            self._mean_both = (self.mean_left + self.mean_right) / 2
        else:
            self._mean_both = None

    def __str__(self):
        return 'Left: {0} Right: {1} Both: {2}'.format(
            self.mean_left, self.mean_right, self._mean_both)


class SubjectPupil(PupilMetric):
    def __init__(self, data_left, data_right, timestamps, k=20, mode='center'):
        super().__init__()
        self.filter_k = k
        self.filter_mode = mode
        self.actual_step = None
        self.pupil_info_by_step = []
        self.data_l = []
        self.data_r = []

        self.timestamps = timestamps

        self.apply_filter(data_left, data_right)

        self.set_means()

    def apply_filter(self, data_l, data_r):
        raise Exception('Need to be overridden in child class')

    def process_data_by_steps(self, steps_pupil, video_start_time):
        last_index = 0
        _length = len(self.timestamps)

        for step_p in steps_pupil:
            actual = self.new_step(step_p.name)

            for i in range(last_index, _length):
                timestamp, d_left, d_right = self.timestamps[i], self.data_l[i], self.data_r[i]
                video_time = timestamp - video_start_time

                if step_p.start < video_time < step_p.end:
                    actual.add_logs(d_left, d_right)

                elif step_p.end < video_time:  # next step while keeping tracks of position
                    last_index = i

                    self.validate_actual()
                    break

    def new_step(self, id):
        self.actual_step = StepInfo(id)
        return self.actual_step

    def validate_actual(self):
        self.actual_step.validate()
        self.pupil_info_by_step.append(self.actual_step)

        self.actual_step = None

    def set_means(self):
        self.mean_left = mean(self.data_l)
        self.mean_right = mean(self.data_r)
        self._set_both()

    def set_multipliers(self):

        for stp in self.pupil_info_by_step:
            if not stp.is_valid():
                continue

            stp.multi_l = stp.left.mean / self.mean_left
            stp.multi_r = stp.right.mean / self.mean_right

    def standardization(self):
        """
        NEED TO DIVIDE BY STANDARD DEVIATION

        :return:
        """
        # use mean to standardize the data around 0.0, wil be easier to compare every steps
        # use subject mean or step mean ???
        # special case : standard deviation = 0 -> no division

        for stp in self.pupil_info_by_step:
            _left, _right = stp.left, stp.right

            if _left.is_valid():
                _left.standardization(_left.mean, _left.sd)
            if _right.is_valid():
                _right.standardization(_right.mean, _right.sd)

    def __str__(self):
        return 'SubjectPupil : {0}'.format(super().__str__())


class SPHampel(SubjectPupil):
    def __init__(self, data_left, data_right, timestamps, k=20, mode='center'):
        super().__init__(data_left, data_right, timestamps, k, mode)

    def apply_filter(self, data_l, data_r):
        """ Using Hampel filter """
        self.data_l = self.apply_hampel_filter(data_l)
        self.data_r = self.apply_hampel_filter(data_r)

    def apply_hampel_filter(self, data):
        items = np.array(data)
        _new, _bool = hampel(items, self.filter_k, self.filter_mode)  # need np.array

        return _new


def _exclude_detection_error(items):
    return [item for item in items if item > 0.0]  # list(filter(-1.0, items)) ?


def _replace_detection_error(items, val):
    return [item if item > 0.0 else val for item in items]


def _identical_values(val, length):
    a = np.empty(length)
    a[:] = val
    return a


def _pupillograms(data, intervals_length, step_name, out_dir):
    """ Generate graph """

    _xlabel = 'Data logs (milliseconds)'
    _title = '{0} pupil diameters'.format(step_name)
    _path = join(out_dir, '{0}_pupillograms.jpg'.format(step_name))
    # log number to milliseconds
    _intervals = range(0, intervals_length*TIME_BETWEEN_LOGS, TIME_BETWEEN_LOGS)

    single_metric_graph(size=(19.2, 7.2),
                        xlabel=_xlabel,
                        data=data,
                        intervals=_intervals,
                        title=_title,
                        path=_path)
                        # yticks=np.arange(-1.1, 1.1, step=0.2))


def generate(log_path):
    print(log_path)
    tobii_path = find_tobii_file(log_path, PREFIX_TOBII)
    steps_path = join(log_path, STEPS_TEXT_FILE_NAME)
    video_times = read_screen_record_timestamp_file(join(log_path, VIDEO_TIMESTAMP_FILE_NAME))
    ref_time = video_times[0]

    _steps_pupil = load_steps_info_file(steps_path)

    ''' 
    Retrieve pupil diameters data and generate a graph per eye
    -> show data by step 
    '''
    pupil_data = pupil_sizes_from_file(tobii_path)

    '''     Generate:    
    1)  Synchronizing
    2)  data without detection errors
    3)  mean
    4)  data where detection errors were replaced by mean value
    '''
    index = 0
    for i in range(len(pupil_data)):
        time, l, r = pupil_data[i]
        if ref_time < time:
            index = i
            break

    pupil_data = pupil_data[index:len(pupil_data)]
    times, p_left, p_right = zip(*pupil_data)

    _cleaned_l, _cleaned_r = _exclude_detection_error(p_left), _exclude_detection_error(p_right)
    m_l, m_r = mean(_cleaned_l), mean(_cleaned_r)
    _mean_l, _mean_r = _identical_values(m_l, len(p_left)), _identical_values(m_r, len(p_right))

    def _generate_subject_info(k=20, mode='center'):
        """
        First, retrieve data for each steps
        """
        subject = SPHampel(p_left, p_right, times, k, mode)
        subject.process_data_by_steps(_steps_pupil, ref_time)
        subject.standardization()

        ''' Graph by step'''
        pupil_dir = join(log_path, 'pupil_{0}_{1}'.format(k, mode))
        os.makedirs(pupil_dir, exist_ok=True)

        for stp in subject.pupil_info_by_step:
            if not stp.is_valid():
                continue

            l, r = stp.left.z_score, stp.right.z_score

            _pupillograms([('left', l), ('right', r)], len(l), stp.name, pupil_dir)

        ''' Generate graph '''
        # _step_intervals = [(round(s.start*EYE_TRACKER_FREQUENCY), round(s.end*EYE_TRACKER_FREQUENCY))
        #                    for s in _steps_pupil]

        # _size = (19.2, 7.2)
        # _xlabel = 'Data logs (milliseconds)'
        # _title = 'Pupil diameters'

        # Left
        # _data = [('data', subject.data_l), ('mean', _mean_l)]
        # single_metric_graph(size=_size,
        #                     xlabel=_xlabel,
        #                     data=_data,
        #                     intervals=range(len(_mean_l)),
        #                     title=_title,
        #                     path=join(pupil_dir, 'Left_pupil_diameters.jpg'),
        #                     special_intervals=_step_intervals)
        #
        # # Right
        # _data = [('data', subject.data_r), ('mean', _mean_r)]
        # single_metric_graph(size=_size,
        #                     xlabel=_xlabel,
        #                     data=_data,
        #                     intervals=range(len(_mean_r)),
        #                     title=_title,
        #                     path=join(pupil_dir, 'Right_pupil_diameters.jpg'),
        #                     special_intervals=_step_intervals)
        return subject

    # _generate_subject_info(10)
    # _generate_subject_info(30)
    # _generate_subject_info(40)
    # _generate_subject_info(50)

    return _generate_subject_info()


def generate_graph_comparing_groups(groups_info, group1, group2, path, size=(19.2, 10.8), bar_width=0.35, opacity=0.8):

    steps_name1, m_1, std_d_1 = zip(*groups_info[group1])
    steps_name2, m_2, std_d_2 = zip(*groups_info[group2])
    # create plot
    fig = plt.figure(figsize=size)
    fig.add_subplot()
    index = np.arange(len(steps_name1))

    plt.bar(index, m_1, bar_width,
            alpha=opacity,
            color='steelblue',
            label=group1)
    y = m_1
    e = np.array(std_d_1)
    plt.errorbar(index, y, e, linestyle='None', fmt='o', ecolor='red', color='black', capsize=0)

    index_expert = index + bar_width
    plt.bar(index_expert, m_2, bar_width,
            alpha=opacity,
            color='burlywood',
            label=group2)
    y = m_2
    e = np.array(std_d_2)
    plt.errorbar(index_expert, y, e, linestyle='None', fmt='o', ecolor='red', color='black', capsize=0)

    font = {'family': 'serif',
            'color': 'darkred',
            'weight': 'normal',
            'size': 16,
            }

    plt.xlabel('Steps', font)
    plt.ylabel('Pupil size (percent)', font)
    plt.title('Scores by group for each steps')
    plt.xticks(index + (bar_width / 2), steps_name1, rotation=0)

    plt.axhline(1, color='black', linestyle=':')

    plt.legend()

    plt.tight_layout()
    plt.savefig(path)
    # plt.show()
    plt.close()


def generate_all(dir_path):
    group_name = dir_path.split('/')[-1]
    list_logs_dir = get_acquisition_logs_only(dir_path)

    if len(list_logs_dir) == 0:
        raise Exception('No acquisition log was found in the directory: {0}.'.format(dir_path))

    subjects = [generate(_dir) for _dir in list_logs_dir]

    [s.set_multipliers() for s in subjects]

    info_by_step = {}
    for i in range(len(subjects[0].pupil_info_by_step)):
        name = subjects[0].pupil_info_by_step[i].name

        info_by_step[name] = [s.pupil_info_by_step[i] for s in subjects]

    _data = []
    for step_name in sorted(info_by_step):
        steps = info_by_step[step_name]

        ############################################
        _data_l, _data_r = [], []
        for i in range(len(steps)):
            step = info_by_step[step_name][i]
            number = i+1

            def add_data(array, data, id='subject {0:03d}'.format(number)):
                if data.is_valid():
                    array.append((id, data.z_score))

            add_data(_data_l, step.left)
            add_data(_data_r, step.right)

        def graph_step_by_group(data, title):
            common_length = min([len(d) for n, d in data])
            _data_same_len = [(n, d[:common_length]) for n, d in data]

            single_metric_graph(size=(19.2, 7.2),
                                xlabel='z scores',
                                data=_data_same_len,
                                intervals=range(common_length),
                                title='Pupil Dilatation interval ({0})'.format(group_name),
                                path=join(dir_path, title + '.jpg'),
                                yticks=np.arange(-7.0, 7.0, step=1.0))

        graph_step_by_group(_data_l, '{0}_Left'.format(step_name))
        graph_step_by_group(_data_r, '{0}_Right'.format(step_name))

        ##################################

        print(step_name)
        
        #  only valid steps are used
        bunch = [(step.left.mean, step.right.mean, step.left.sd, step.right.sd) for step in steps if step.is_valid()]
        multi_l, multi_r, sd_l, sd_r = zip(*bunch)

        mean_dia_l = mean(multi_l)
        mean_dia_r = mean(multi_r)
        mean_sd_l = mean(sd_l)
        mean_sd_r = mean(sd_r)

        print('Means by step : {0} | {1}'.format(mean_dia_l, mean_dia_r))
        print('Standard deviation : {0} | {1}'.format(mean_sd_l, mean_sd_r))

        _data.append((step_name, mean([mean_dia_l, mean_dia_r]), mean([mean_sd_l, mean_sd_r])))

    return _data

if __name__ == '__main__':

    # log directory: group 1
    path1 = ''
    # log directory: group 2
    path2 = ''
    output = 'pupil_size_per_group_per_step.png' # by default, in current directory
    checkDir(path1)
    checkDir(path2)

    group1 = generate_all(path1)
    group2 = generate_all(path2)

    group_info = {'sound_off': group1, 'sound_on': group2}

    generate_graph_comparing_groups(group_info, 'sound_off', 'sound_on', output)
