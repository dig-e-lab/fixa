<img align="right" src="images/logo_digElab.png">

### FixA : fixation analysis 

FixA is a set of scripts which compute metrics from eye tracking data, especially fixations.

### How to use FixA:

You have to fill a configuration file with correct values in order to use the following scripts. For further references, please check the /Template/config.json file.

To use FixA, you need to organize your data into two groups of data. 
For example, gather the data of the people wearing glass (ex: glass) in one group and the data not wearing glass in another group (ex: no_glass). 
If you can't or dont want to do that : you still have to create a subdirectory in the 'FIXATIONS' folder and put all your fixations in it. BUT, keep in mind that, you have to set the parameter "ref_group" with the name of the subdirectory. Be aware that, you will not have access to all the metrics by doing this.


1 - Use CSR and Animation sofware to get all the data you need or run your own aquisition software (if the output format is compatible)

2 - Run the script Process_acquisition_logs.py

3 - Run Frame_extractor.py to retreive common pictures/steps from the acquisition videos then use aoi.py to split each steps into AOI (area of interest)

Note : do not forget to move/copy the steps into your 'STIMULI' folder before going further.


4 - Use Generate_AOI.py to generate the area of interest for each steps

5 - Use Generate_common_steps.py, Generate_steps.py or Generate_special_interval.py based on your needs.

Note : do not forget to move/copy the fixations BY GROUP into your 'FIXATIONS' folder before going further. 


6 - Run Maps.py to get the fixation maps and saliency maps.

7 - Once you have the fixations for each step, the fixation maps and saliency maps, you can run Analysis.py.


### Requirements:

FixA was developped with Python 3.5.4 on Windows 7/ 10 but was also tested on Python 3.6.8 . 

Please use the correct requirement file based on your Python version :

	- 3.5.4 : requirement.txt

	- 3.6.8 : requirement_36.txt


### Main scripts:

- Process_acquisition_logs.py
	
If you are using camera-based eye tracking software which only give you the gaze direction and not the gaze point on screen, this script can convert gaze direction into gaze point.
To use this script, you have to use the animation software(Animation.exe) at the beginning of each experiment.

This script can also generate a video that show the gazepoints for each device used during the experiment and, last but not least, it creates the mandatory folders in the directory precised by "output_analysis_dir" in the configuration file.

Mandatory folders:

	- AOI
	- FIXATION_MAPS
	- FIXATIONS
	- SALIENCY_MAPS
	- STIMULI


- Frame_extractor.py

Allows you to extract a frame from a video such as the recorded screen during the experiment(using CSR or another capture software). 
Then, you can use this frame as a step for the analysis by placing it in the 'STIMULI' directory (see config.json for more info on "step").


- Generate_common_steps.py

Extract frames that can be used as step from every videos in the acquisitions. Then check similarity to find common steps between every acquisition logs.

Be aware that :

	- this script can take a lot of time based on your dataset 
	- if the script failed to find fixations for every subject regarding a step, the script will stop right away


- Generate_steps.py

Search for each step placed in the 'STIMULI' directory in the acquisition videos and isolate the fixations during the time when the frame is visible/detected in the video (first occurrence only).


- Generate_special_interval.py

Allows you to isolate a part of the video that is composed of more than one step and retreive the fixations.


- Maps.py

Use this script to generate the saliency maps and fixation maps from the fixation files(.fix).


- Analysis.py

Provides fixation-based metrics on your data :

	- Euclidean distance
	- opacity maps
	- AOI

If you can divide your data into 2 groups with one on them as "reference" group. The script can provide much more metrics: 

	- String edit distance
	- Time delay edit distance
	- NSS
	- KL-divergence
	- AUC Judd

Part of these metrics come from FixaTons (check the "Citations" section).


- Generate_AOI.py

Use this script to split the step into AOI (area of interest). It will allow you to know the repartition/dispersion of the fixations on a given stimulus/step.

![alt text1](images/example.png) ![alt text2](images/example_result.png)


- Cognitive.py

You can use this script if you want to add pupil study to your analysis. This script will generate some graphs based on the pupil diameter variation during a step.



### What is a "Step" ?

A step is a frame/picture that is visible for a limited time within a video and for which we want to know the repartition of the fixations.

![alt text3](images/stimulus_seg.png)


### CSR

You can find the installer(msi file) and the manual in the CSR directory.


### Animation.exe

You can find the Animation.exe in the CSR directory. No installation is needed but it's recommended to install CSR to ensure that basic .NET library is installed.

In order to use it. You have to specify 3 parameters :

- output directory
- number of calibration point (4 or 9)
- the number of the screen you want to show the animation

It's recommended to create a shortcut and add these parameters after the path in "Start in" property.


### Citations

- FixaTons

Dario Zanca, FixaTons, (2017), GitHub repository, 
https://github.com/dariozanca/FixaTons


FixaTons technical report, 
https://arxiv.org/abs/1802.02534

and also, all the correspondent publications for the dataset included(MIT1003),
http://people.csail.mit.edu/tjudd/WherePeopleLook/Docs/wherepeoplelook.pdf



### Instructions (en français)

Deux outils majeurs ont été développés tout au long du projet Dig-e-Lab: CSR et FixA.

L’outil CSR permet d’enregistrer plusieurs flux de données de manière simultanée. Les flux de données en question sont : l’affichage de l’écran, le flux vidéo de la webcam ainsi que les données d’un oculomètre de la marque Tobii.

L’outil CSR peut être installé sur tout ordinateur fonctionnant sous Windows 7 ou Windows 10. Son utilisation est relativement simple. Un manuel est également disponible.


Quant à FixA, il s’agit d’un outil d’analyse de données de suivi du regard (eye tracking), comme les données de sorties de l’outil CSR. 
L’outil FixA permet, entre autres, d’étudier la position du regard durant toute la durée de l’expérience ou uniquement lors de passages spécifiques (configuration).

Comme ces données sont acquises lors d’expériences menées de manière spécifique, de solides connaissances dans le domaine de l’étude du suivi du regard sont nécessaires pour utiliser correctement les outils.

L’utilisation de FixA est plus compliquée que celle de CSR. Des connaissances en programmation en langage Python et en langue anglaise sont requises. Pouvoir lancer des scripts en ligne de commande ainsi que pouvoir modifier/comprendre un fichier au format json sont les compétences minimales pour utiliser FixA.
Comme l’outil est en réalité un ensemble de scripts écrit en langage Python, il est nécessaire d’installer une version de Python compatible avec FixA sur votre machine. Une fois l’installation de Python terminée, il restera encore toutes les dépendances de FixA à installer. Ces dernières sont listées dans un fichier à la base du répertoire (requirements.txt).
Ensuite, vous pourrez utiliser les scripts de FixA comme n’importe quel script python via une console de commande. 

Le manuel d’utilisation de FixA (disponible en français dans le dossier 0_FR) couvre uniquement son utilisation, pas la mise en place d’un environnement Python.
