from common import *

from Models.step import dump_steps, load_steps_info_file

from Modules.steps_from_video import full_screen_video_detection
from Modules.file_utils import get_acquisition_logs_from_group_dir, read_config
from Modules.raw_data_manipulation import retrieve_fixations_by_steps


if __name__ == '__main__':

    """
    Isolate a part of the video using start.png and stop.png
    This part will therefore be considerate as an unique step
    """
    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    _config = read_config(_config_file)
    _dir = _config.acquisition_logs_dir
    start_stop_dir = _config.steps.start_stop_dir

    list_logs_dir = get_acquisition_logs_from_group_dir(_dir)
    _id = 1  # use to identify log file
    'WARNING: the ids are shared between all groups due to the method: get_acquisition_logs_from_group_dir'

    if len(list_logs_dir) == 0:
        raise Exception('No acquisition log was found in the directory: {0}.'.format(_dir))

    for _logs_dir in sorted(list_logs_dir):

        v_in = join(_logs_dir, VIDEO_FILE_NAME)
        v_timestamps = join(_logs_dir, VIDEO_TIMESTAMP_FILE_NAME)
        _full_screen = join(_logs_dir, 'Step_full_screen.txt')

        _start = join(start_stop_dir, 'start.png')
        _end = join(start_stop_dir, 'stop.png')

        print(_logs_dir)

        if not exists(_full_screen):

            full_screen_step = full_screen_video_detection(v_in, v_timestamps, _start, _end)
            steps = [full_screen_step, ]
            dump_steps(_full_screen, steps)

        else:
            print('File already exist : {0}'.format(_full_screen))
            steps = load_steps_info_file(_full_screen)

        retrieve_fixations_by_steps(_logs_dir, join(_logs_dir, PREFIX_TOBII + FIXATIONS_FILE_EXTENSION),
                                    steps, file_complement='_{0}'.format(_id))

        _id += 1
