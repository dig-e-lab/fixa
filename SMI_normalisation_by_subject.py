import argparse
import os

from pathlib import Path


def _read_smi_data(source, header=True):
    from_file, _header = [], None

    with open(source, 'r') as file:
        for line in file:
            if _header is None and header:
                _header = line
                continue

            data = line.split('\t')
            from_file.append(LogSMI(data))

    return _header, from_file


class LogSMI:
    def __init__(self, data):
        self.trial = str(data[0])
        self.subject = str(data[1])
        self.color = str(data[2])
        self.stimulus = str(data[3])
        self.start_time = float(data[4])
        self.end_time = float(data[5])
        self.fixation_start = float(data[6])
        self.fixation_duration = float(data[7])
        self.fixation_end = float(data[8])
        self.x = float(data[9])
        self.y = float(data[10])
        self.pupil_size_x = float(data[11])
        self.pupil_size_y = float(data[12])
        self.av_pupil_diameter = float(data[13])
        self.dispersion_x = float(data[14])
        self.dispersion_y = float(data[15])
        self.left_right = str(data[16])
        self.number = int(data[17])
        self.aoi_name = str(data[18])


if __name__ == '__main__':
    """ Certify experiment of the same duration to always compare every subject
    (based on the shortest time)
    """

    parser = argparse.ArgumentParser(description='Demonstration of landmarks localization.')
    parser.add_argument('-v', type=str, help='logging level', default='info',
                        choices=['debug', 'info', 'warning', 'error', 'critical'])
    parser.add_argument('--file', type=str, help='Path of the directory that contains SMI files.')
    parser.add_argument('--cut', type=bool, default=True,
                        help='Normalize time for every subject based on the fastest subject')
    args = parser.parse_args()

    source = str(args.file) # SMI eye tracker output
    
    cut = bool(args.cut)
    cut = False

    dir_file = os.path.dirname(os.path.realpath(source))
    file_name = Path(source).stem

    # first, get the data

    header, from_file = _read_smi_data(source)

    # get shortest trial time (fastest subject time)
    shortest_time = 0.0
    if cut:
        for data in from_file:

            if shortest_time == 0:
                shortest_time = data.end_time - data.start_time
                continue

            tmp = data.end_time - data.start_time
            if shortest_time > tmp:
                shortest_time = tmp

        print('shortest trial time (fastest subject time): ' + str(shortest_time))

    # limit log by subject based on the shortest time
    normalised_data = []
    actual_subject, limit_duration, file_cut = None, None, None
    extension = '_cut' if cut else ''

    for data in from_file:

        if actual_subject != data.subject:

            if file_cut is not None:
                file_cut.close()

            actual_subject = data.subject
            limit_duration = data.start_time + shortest_time
            file_cut = open(os.path.join(dir_file, '{0}{1}.txt'.format(actual_subject, extension)), 'w')

        if cut and data.fixation_start > limit_duration:
            continue

        normalised_data.append(data)
        file_cut.write(str(data))

    if cut:
        dest = os.path.join(dir_file, '{0}{1}.txt'.format(file_name, extension))

        with open(dest, 'w') as file:

            # file.write(header)  # add column name from source file

            for nd in normalised_data:
                file.write(str(nd))
