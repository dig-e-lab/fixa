from common import *

from Modules.process_calibration import main as eye_tracker_raw_data_generator
from Modules.raw_data_manipulation import map_raw_data, save_merged_fix, process_raw_into_fixations
from Modules.file_utils import read_screen_record_timestamp_file, get_acquisition_logs_from_group_dir, \
    find_tobii_file, convert_tobii_file, read_config
from Modules.video_gaze import generate_video
from Modules.perf_image import generate_performance_image_from_dir, TOBII_LOGS_FILE_NAME
from Modules.utils import timestamp_to_ticks, get_color


def generate(log_path, video):
    tobii_logs_path = join(log_path, TOBII_LOGS_FILE_NAME)
    gazeml_logs_path = join(log_path, CARTESIAN_LOGS_FILE_NAME)
    video_times = read_screen_record_timestamp_file(join(log_path, VIDEO_TIMESTAMP_FILE_NAME))

    '''1 - generates camera-based eye tracker gaze points (raw data)'''

    eye_tracker_raw_data_generator(log_path)

    '''2 - convert tobii logs (generics)'''
    tobii_path = find_tobii_file(log_path, PREFIX_TOBII)
    convert_tobii_file(tobii_path, tobii_logs_path)

    '''3 - generate performance results (image)'''
    generate_performance_image_from_dir(log_path)

    '''4 - generate fixation files'''
    if exists(tobii_logs_path):
        data_tobii = process_raw_into_fixations(tobii_logs_path, video_times[0])
        save_merged_fix(join(log_path, PREFIX_TOBII + FIXATIONS_FILE_EXTENSION),
                        data_tobii)

    if exists(gazeml_logs_path):
        data_gazeml = process_raw_into_fixations(gazeml_logs_path, video_times[0])
        save_merged_fix(join(log_path, PREFIX_GAZEML + FIXATIONS_FILE_EXTENSION),
                        data_gazeml)

    '''5 - optional - generate video with gazes from each devices'''

    if video:
        list_points = []
        video_name = join(log_path, VIDEO_FILE_NAME)
        gazeml_logs = join(log_path, CARTESIAN_LOGS_FILE_NAME)
        gazes_paths = [join(log_path, TOBII_LOGS_FILE_NAME), ]

        if exists(gazeml_logs):
            gazes_paths.append(gazeml_logs)

        gazes = map_raw_data(gazes_paths, video_times)

        for i in range(len(gazes)):
            list_points.append(([(gp.x, gp.y) for gp in gazes[i]], get_color(i)))

        generate_video(video_name, join(log_path, VIDEO_GAZE_POINTS_FILE), list_points)


def generate_all(dir_path, video=True):

    list_logs_dir = get_acquisition_logs_from_group_dir(dir_path)

    if len(list_logs_dir) == 0:
        raise Exception('No acquisition log was found in the directory: {0}.'.format(dir_path))

    [generate(_dir, video) for _dir in list_logs_dir]


if __name__ == '__main__':

    """
    Main script used to process acquisition data
    """
    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    _config = read_config(_config_file)

    generate_all(_config.acquisition_logs_dir, _config.video_gaze)

    ''' Creates folders in the analysis directory 
    '''
    _analysis_dir = _config.output_analysis_dir
    os.makedirs(join(_analysis_dir, ANALYSIS_STIMULI_DIR), exist_ok=True)
    os.makedirs(join(_analysis_dir, ANALYSIS_AOI_DIR), exist_ok=True)
    os.makedirs(join(_analysis_dir, ANALYSIS_FIXATIONS_DIR), exist_ok=True)
    os.makedirs(join(_analysis_dir, ANALYSIS_FIXATION_MAPS_DIR), exist_ok=True)
    os.makedirs(join(_analysis_dir, ANALYSIS_SALIENCY_MAPS_DIR), exist_ok=True)

