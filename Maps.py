from common import *

from Modules.maps_utils import generate_saliency_map, generate_fixations_map
from Modules.file_utils import get_files_with_extension, read_config
from Modules.utils import simple_list_from_2dmatrix

from Models.fixation import read_fixation_file


if __name__ == '__main__':
    '''
    Generate fixation map and saliency map for each steps and by group
    '''
    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    config = read_config(_config_file)
    config_maps = config.maps
    fixations_dir = config_maps.fixations_dir
    fm_dir = config_maps.output_fm_dir
    sm_dir = config_maps.output_sm_dir

    step_names = [file.split('.')[0] for file in os.listdir(config.steps.stimuli_dir)]

    for r, d, f in os.walk(fixations_dir):

        if r is not fixations_dir:
            break

        for _r, _d, _f in os.walk(join(r, fixations_dir)):
            for _group in _d:
                files = get_files_with_extension(join(_r, _group), [FIXATIONS_FILE_EXTENSION, ])
                print('Group: {0}'.format(_group))

                for _step in step_names:
                    print('Starting Step : {0}'.format(_step))

                    _step_fm = join(fm_dir, _step)
                    os.makedirs(_step_fm, exist_ok=True)

                    _step_sm = join(sm_dir, _step)
                    os.makedirs(_step_sm, exist_ok=True)

                    files_for_step = [f for f in files if _step in f]

                    ''' Read each fixation files '''
                    users = [read_fixation_file(file) for file in files_for_step]

                    def _get_coord(fix):
                        return round(fix.x), round(fix.y)

                    all_subject_data = simple_list_from_2dmatrix(users, _get_coord)

                    generate_fixations_map(all_subject_data, width=1920, height=1080,
                                           file_name=join(_step_fm, _group))

                    generate_saliency_map(all_subject_data, file_name=join(_step_sm, _group),
                                          width=1920, height=1080)
