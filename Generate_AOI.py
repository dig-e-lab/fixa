import cv2
import pickle

from Models.aoi import AOI, get_areas_text_default_values, show_areas_on_image, dump_areas

if __name__ == '__main__':

    # Example
    #_aoi = [AOI('A', (0, 0), (630, 880)),
    #          AOI('B', (630, 0), (1650, 880)),
    #          AOI('C', (1650, 0), (1920, 880)),
    #          AOI('D', (0, 880), (1920, 1080))]

    _aoi = [AOI('A', (0, 0), (1920, 880)),
              AOI('B', (0, 880), (1920, 1080))]
    """
    (x1, y1)
        +---------+---------+
        |                   |
        |        "A"        |
        |                   |
(x1, y2)+---------+---------+ (x2, y2)
        |        "B"        |
        +-------------------+ (x2, y3)
                                    
        =>  AOI('A', (x1, y1), (x2, y2))
        =>  AOI('B', (x1, y2), (x2, y3))
    
    """
    # path of the folder containing the picture you want to use
    _path = ''
    # name of the picture without extension
    _name = ''
    tmp = cv2.imread(_path + _name + '.png', 1)
    tmp = cv2.cvtColor(tmp, cv2.COLOR_RGB2BGR)

    di = get_areas_text_default_values(_aoi)
    show_areas_on_image(tmp, _aoi, di, _path + _name + '_aoi.png')
    dump_areas(_path + _name + '.json', _aoi)
