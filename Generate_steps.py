from common import *

from Models.step import dump_steps, load_steps_info_file

from Modules.steps_from_video import get_slicer_based_on_mode
from Modules.file_utils import get_acquisition_logs_from_group_dir, get_files_with_extension, \
    read_screen_record_timestamp_file, read_config
from Modules.raw_data_manipulation import retrieve_fixations_by_steps

from Modules.poolRunner import PoolRunner

ssim_mode = 'ssim'
hash_mode = 'hash'

def _run_pool(process, data_by_process):
    _pr = PoolRunner(process)
    _pr.run_pool(data_by_process)


def run_slicer(slicer, log_dir, id):

    v_in = join(log_dir, VIDEO_FILE_NAME)
    v_timestamps = join(log_dir, VIDEO_TIMESTAMP_FILE_NAME)
    _step_file = join(log_dir, 'Steps.txt')
    ref_time = read_screen_record_timestamp_file(v_timestamps)[0]

    print(log_dir)
    if not exists(_step_file):
        steps = slicer.run(v_in, v_timestamps)
        dump_steps(_step_file, steps)
    else:
        print('File already exist : {0}'.format(_step_file))
        steps = load_steps_info_file(_step_file)

    retrieve_fixations_by_steps(log_dir, join(log_dir, PREFIX_TOBII + FIXATIONS_FILE_EXTENSION),
                                steps, file_complement='_{:02d}'.format(id))


if __name__ == '__main__':

    """
    Detects if the pictures(steps) from a specific folder are part of a video
    """
    _config_file = ProcessConfigFileArgs()
    
    if not _config_file:
        raise Exception('No configuration file detected')

    _config = read_config(_config_file)
    _dir = _config.acquisition_logs_dir
    _steps_dir = _config.steps.stimuli_dir
    _by_picture = _config.steps.threshold_by_picture
    _detection_mode = _config.steps.mode
    _threshold = _config.steps.threshold

    list_logs_dir = sorted(get_acquisition_logs_from_group_dir(_dir))
    _id = 1  # use to identify log file
    'WARNING: the ids are shared between all groups due to the method: get_acquisition_logs_from_group_dir'

    if len(list_logs_dir) == 0:
        raise Exception('No acquisition log was found in the directory: {0}.'.format(_dir))

    img_paths = get_files_with_extension(_steps_dir, extensions=['.png', ])

    if len(img_paths) == 0:
        raise Exception('No images in the step folder.')
    _slicer = get_slicer_based_on_mode(_detection_mode, img_paths, _by_picture)

    _tuple = []
    for _logs_dir in list_logs_dir:
        _dir_slicer = get_slicer_based_on_mode(_detection_mode, img_paths, _by_picture)
        _index = list_logs_dir.index(_logs_dir)
        _tuple.append((_dir_slicer, _logs_dir, _index))
    
    _run_pool(run_slicer, _tuple)
